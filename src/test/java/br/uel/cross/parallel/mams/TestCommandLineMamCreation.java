package br.uel.cross.parallel.mams;

import br.uel.cross.parallel.mams.distance.EuclideanMetricEvaluator;
import br.uel.cross.parallel.mams.pagemanager.*;
import br.uel.cross.parallel.mams.split.MinMaxPromoter;
import br.uel.cross.parallel.mams.split.MstSplitterPromoter;
import br.uel.cross.parallel.mams.split.RandomPromoter;
import br.uel.cross.parallel.mams.subtreechooser.BiasedSubTreeChooser;
import br.uel.cross.parallel.mams.subtreechooser.MinDistSubTreeChooser;
import br.uel.cross.parallel.mams.subtreechooser.MinOccupancySubTreeChooser;
import org.apache.commons.cli.CommandLine;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestCommandLineMamCreation {

    private CommandLineHandler commandLineHandler;

    @Mock
    private CommandLine cmd;

    public TestCommandLineMamCreation() {

        MockitoAnnotations.initMocks(this);
        commandLineHandler = new CommandLineHandler(cmd);
    }

    @Test
    public void mamDefaultValues() {

        MamTree tree = commandLineHandler.buildTree();
        assertThat(tree.getPageManager(), instanceOf(MemoryPageManager.class));
        assertThat(tree.getPromoter(), instanceOf(MstSplitterPromoter.class));
        assertThat(tree.getMetricEvaluator(), instanceOf(EuclideanMetricEvaluator.class));
        assertThat(tree.getSubtreeChooser(), instanceOf(MinDistSubTreeChooser.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void mamPageManagerValues() {

        insertMockCallsToCommandLine("pm", "File");
        MamTree tree = commandLineHandler.buildTree();
        assertThat(tree.getPageManager(), instanceOf(FilePageManager.class));

        insertMockCallsToCommandLine("pm", "abc");
        commandLineHandler.buildTree();

//        TODO Testar se é HDFS sem precisar instanciar o objeto

        insertMockCallsToCommandLine("c", "20");
        tree = commandLineHandler.buildTree();
        assertThat(tree.getPageManager(), instanceOf(GuavaCachePageManagerDecorator.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void mamSubTreeValues() {
        insertMockCallsToCommandLine("sub", "MinOccup");
        MamTree tree = commandLineHandler.buildTree();
        assertThat(tree.getSubtreeChooser(), instanceOf(MinOccupancySubTreeChooser.class));

        insertMockCallsToCommandLine("sub", "MinDist");
        tree = commandLineHandler.buildTree();
        assertThat(tree.getSubtreeChooser(), instanceOf(MinDistSubTreeChooser.class));

        insertMockCallsToCommandLine("sub", "Biased");
        tree = commandLineHandler.buildTree();
        assertThat(tree.getSubtreeChooser(), instanceOf(BiasedSubTreeChooser.class));

        insertMockCallsToCommandLine("sub", "abc");
        commandLineHandler.buildTree();
    }

    @Test
    public void mamPromoterValues() throws Exception {
        insertMockCallsToCommandLine("s", "Mst");
        MamTree tree = commandLineHandler.buildTree();
        assertThat(tree.getPromoter(), instanceOf(MstSplitterPromoter.class));

        insertMockCallsToCommandLine("s", "MinMax");
        tree = commandLineHandler.buildTree();
        assertThat(tree.getPromoter(), instanceOf(MinMaxPromoter.class));

        insertMockCallsToCommandLine("s", "Random");
        tree = commandLineHandler.buildTree();
        assertThat(tree.getPromoter(), instanceOf(RandomPromoter.class));
    }

    @Test
    public void mamDistanceValues() throws Exception {
        insertMockCallsToCommandLine("d", "Euclidiana");
        MamTree tree = commandLineHandler.buildTree();
        assertThat(tree.getMetricEvaluator(), instanceOf(EuclideanMetricEvaluator.class));
    }

    private void insertMockCallsToCommandLine(String parameter, String value) {
        when(cmd.hasOption(parameter)).thenReturn(true);
        when(cmd.getOptionValue(parameter)).thenReturn(value);
    }

}
