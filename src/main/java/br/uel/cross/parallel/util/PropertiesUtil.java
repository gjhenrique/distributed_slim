package br.uel.cross.parallel.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {

	public static int getIntFromConfiguration(String confFile, String key) {

		String str = getValueFromConfiguration(confFile, key);

		return Integer.parseInt(str);
	}

	public static double getDoubleFromConfiguration(String confFile, String key) {

		String str = getValueFromConfiguration(confFile, key);

		return Double.parseDouble(str);
	}

	public static String getValueFromConfiguration(String confFile, String key) {
		Properties networkProperties = new Properties();

		String stringValue = null;
		try {

			networkProperties.load(new FileInputStream(confFile));

			stringValue = networkProperties.getProperty(key);

			if (stringValue == null) {
				throw new RuntimeException("Valor para " + key + " no arquivo " + confFile + " não existe");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringValue;
	}
}
