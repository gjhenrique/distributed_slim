package br.uel.cross.parallel.util;

public class EntrySizeInfo {

	private int entrySize;
	
	private int objectHeader;

	public EntrySizeInfo(int entrySize, int objectHeader){
		this.entrySize = entrySize;
		this.objectHeader = objectHeader;
	}
	
	public int getEntrySize() {
		return entrySize;
	}

	public void setEntrySize(int entrySize) {
		this.entrySize = entrySize;
	}

	public int getObjectHeader() {
		return objectHeader;
	}

	public void setObjectHeader(int objectHeader) {
		this.objectHeader = objectHeader;
	}
	
}
