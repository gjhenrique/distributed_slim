package br.uel.cross.parallel.util;


public class DistanceIndex implements Comparable<DistanceIndex>{
	
	private int index;
	
	private double distance;

	public DistanceIndex(int index, double distance){
		
		this.index = index;
		this.distance = distance;
		
	}
	
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public int compareTo(DistanceIndex o) {
	    return  this.getDistance() < o.getDistance() ? -1
	            : this.getDistance() > o.getDistance() ? 1
	            : 0;
	}

}
