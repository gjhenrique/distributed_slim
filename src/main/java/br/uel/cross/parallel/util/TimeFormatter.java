package br.uel.cross.parallel.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class TimeFormatter {

	public static final String DATE_FORMAT_NOW = "dd/MM/yyy HH:mm:ss.SSS";
	
	public static String formattedTime(long start, long end) {

		long endOfAlgorithm = (end - start) ;
		return formattedTime(endOfAlgorithm);
		
	}
	
	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}
	
	public static String formattedTime(long time){
		
		long micros = TimeUnit.MICROSECONDS.convert(time, TimeUnit.NANOSECONDS);
		long ms = TimeUnit.MILLISECONDS.convert(time, TimeUnit.NANOSECONDS);
		long s = TimeUnit.SECONDS.convert(time, TimeUnit.NANOSECONDS);
		long min = TimeUnit.MINUTES.convert(time, TimeUnit.NANOSECONDS);
//		time = time / 1000000;
//		long ms = time % 1000;
//		long s = (time / 1000) % 60;
//		long min = (time / 1000) / 60;

		String strReturn = min + " min" + " " + s + " s" + " " + ms + " ms " + micros + " micross";
		return strReturn;
	}
}
