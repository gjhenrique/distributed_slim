package br.uel.cross.parallel.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.pagemanager.Serializer;

public class EntrySizeMeasurer {

	public static int defaultBlockSize;

	public static Serializer serializer;

	private static EntrySizeInfo infoOfSerializableLeaf;

	private static EntrySizeInfo infoOfSerializableIndex;

	private static int header = 1;

	public static int getNumberOfLeafEntries(List<EntryTree> entries) {

		if (entries.size() <= 1) {
			return Integer.MAX_VALUE;
		}
		
		if (infoOfSerializableLeaf == null) {
			infoOfSerializableLeaf = getSizeOfSerializedObject(entries);
		}
		
		int numberOfEntries = calculateNumberOfEntries(infoOfSerializableLeaf);

		return numberOfEntries;
	}

	public static int getNumberOfIndexEntries(List<EntryTree> entries) {

		if (entries.size() <= 1) {
			return Integer.MAX_VALUE;
		}

		if (infoOfSerializableIndex == null) {
			infoOfSerializableIndex = getSizeOfSerializedObject(entries);
		}

		return calculateNumberOfEntries(infoOfSerializableIndex);
	}

	public static int getNumberOfIndexEntries() {

		if (infoOfSerializableIndex == null) {
			throw new RuntimeException("Ainda não foi calculado o tamanho dos nós");
		}

		return calculateNumberOfEntries(infoOfSerializableIndex);
	}

	public static int getNumberOfLeafEntries() {

		if (infoOfSerializableLeaf == null) {
			throw new RuntimeException("Ainda não foi calculado o tamanho dos nós");
		}

		return calculateNumberOfEntries(infoOfSerializableLeaf);
	}

	private static int calculateNumberOfEntries(EntrySizeInfo entrySizeInfo) {

		if (entrySizeInfo.getEntrySize() == 0)
			return 0;

		return (defaultBlockSize - (header + entrySizeInfo.getObjectHeader())) / entrySizeInfo.getEntrySize();
	}

	private static EntrySizeInfo getSizeOfSerializedObject(List<EntryTree> entries) {

		int singleElementListSize = returnSerializedListSize(entries, 1);
		int doubleElementListSize = returnSerializedListSize(entries, 2);

		int entrySize = doubleElementListSize - singleElementListSize;
		int objectHeader = singleElementListSize - entrySize;

		return new EntrySizeInfo(entrySize, objectHeader);
	}

	private static int returnSerializedListSize(List<EntryTree> entries, int index) {

		ByteArrayOutputStream bOut = null;

		try {

			bOut = new ByteArrayOutputStream();
			serializer.write(bOut, new ArrayList<EntryTree>(entries.subList(0, index)));

			bOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bOut.toByteArray().length;
	}
}