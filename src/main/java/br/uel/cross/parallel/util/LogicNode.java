package br.uel.cross.parallel.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.model.IndexEntryTree;
import br.uel.cross.parallel.mams.model.LeafEntryTree;
import br.uel.cross.parallel.mams.model.NodeTree;

public class LogicNode {
	
	public enum NodeType{
		LEAFNODE, INDEXNODE
	}
	
	private List<LogicEntry> logicEntries;

    /**
    * Minimum occupation.
    */
    private int minOccupation;

    /**
    * Maximum number of entries.
    */
    private int maxEntries;

    /**
    * Current number of entries.
    */
    private int count;

    /**
    * This vector holds the id of the representative objects.
    */
    private int [] repIndex;

    /**
    * Type of this node.
    */
    private NodeType nodeType;
    
	public LogicNode(int maxOccupation){
		this.maxEntries = maxOccupation;
		
		this.logicEntries = new ArrayList<LogicEntry>();
		
		this.repIndex = new int[2];
		this.repIndex[0] = 0;
		this.repIndex[1] = 0;

		this.count = 0;
		
		this.minOccupation = (int) (0.25 * maxOccupation);

		if( (minOccupation > (maxOccupation / 2)) || minOccupation == 0){
			minOccupation = 2;
		}
	}
    
    /**
    * Verifies if a given object is a representative.
    *
    * @param idx Object index.
    * @return True if the object is a representative or false otherwise.
    */
    public boolean isRepresentative(int idx){
       return ((idx == repIndex[0]) || (idx == repIndex[1]));
    }
      
    public int addEntry(float[] element){
    	
    	if(this.count < this.maxEntries){
    		
    		LogicEntry logicEntry = new LogicEntry();
    		
    		logicEntry.setElement(element);
    		logicEntry.setMine(true);
    		
    		this.logicEntries.add(logicEntry);
    		
    		count++;
    		
    		return count -1;
    	}else{
        	return -1;
    	}
    }
    
    public void addIndexNode(NodeTree node){

    	List<EntryTree> entries = node.getEntry();
    	
    	for(int i = 0; i < entries.size(); i++){
    		
    		int idx = addEntry(entries.get(i).getSpaceElement());
    		
    		IndexEntryTree indexEntry = (IndexEntryTree) entries.get(i);
    		
    		setEntry(idx, indexEntry.getPtr(), indexEntry.getnEntries() , indexEntry.getConveringRadius(), -1);
    	}
    }
    
    public void addLeafNode(NodeTree node){

		List<EntryTree> entries = node.getEntry();

    	for(int i = 0; i < entries.size(); i++){
    		
    		int idx = addEntry(entries.get(i).getSpaceElement());
    		
    		LeafEntryTree leafEntry = (LeafEntryTree) entries.get(i);
    		
    		this.setEntry(idx, 0, 0, 0, leafEntry.getRowId());
    		
    	}
    }
    
    public void setEntry(int idx, long blockId, int nEntries, double radius, long rowId){
    	LogicEntry logicEntry = this.logicEntries.get(idx);
    	logicEntry.setBlockId(blockId);
    	logicEntry.setnEntries(nEntries);
    	logicEntry.setRadius(radius);
    	logicEntry.setRowId(rowId);
    }
    
    public List<LogicEntry> getLogicEntries(){
    	return logicEntries;
    }
    
    public int getCount(){
    	return count;
    }
    
    public void setRepresentatives(int idx1, int idx2){
    	this.repIndex[0] = idx1;
    	this.repIndex[1] = idx2;
    }
    
    public int distribute(NodeTree node0, float[] rep0,
    		NodeTree node1, float[] rep1, MetricEvaluator metricEvaluator){
    	
    	int result = testDistribution(node0, node1, metricEvaluator);
    	
//    	int dimensions = getRepresentativeElement(0).length;
    	
//    	rep0 = new float[dimensions];
//    	rep1 = new float[dimensions];
    	
//    	for(int i = 0; i < dimensions; i++){
//    		rep0[i] = getRepresentativeElement(0)[i];
//    		rep1[i] = getRepresentativeElement(1)[i];
//    	}
//    	rep0.addAll(getRepresentativeElement(0));
//    	rep1.addAll(getRepresentativeElement(1));
    	
    	return result;
    }
    
    public float[] getRepresentativeElement(int idx){
    	return logicEntries.get(repIndex[idx]).getElement();
    }
    
    public int getRepresentativeIndex(int idx){
    	return repIndex[idx];
    }
    
    public int testDistribution(NodeTree node0, NodeTree node1, MetricEvaluator metricEvaluator){
    	
    	int dCount = updateDistances(metricEvaluator);
    	
    	List<DistanceIndex> idx0 = new ArrayList<DistanceIndex>();
    	List<DistanceIndex> idx1 = new ArrayList<DistanceIndex>();
    	
//    	Inicializando os Objetos e setando
    	for(int i = 0; i < count; i++){
    		
    		double distance0 = logicEntries.get(i).getDistance()[0];
    		double distance1 = logicEntries.get(i).getDistance()[1];
    		
    		idx0.add(new DistanceIndex(i, distance0));
    		idx1.add(new DistanceIndex(i, distance1));

    		logicEntries.get(i).setMapped(false);
    	}
    	
//    	Ordenando o vetor pelas distâncias
    	Collections.sort(idx0);
    	Collections.sort(idx1);
    	
//    	Adicionando ao menos os nós da ocupação mínima
    	for(int i = 0; i < minOccupation; i++){
    		
    		this.addEntriesMinOccupation(node0, idx0);
    		this.addEntriesMinOccupation(node1, idx1);
    	}
    	
//		Adicionando o resto dos nós    	
    	for(int i = 0; i < count; i++){
    		
    		if(! logicEntries.get(i).isMapped()){
    			
    			LogicEntry logicIt = getLogicEntries().get(i);
				EntryTree entry = node0.createEntry();
				this.setEntryProperties(entry, i);
				entry.setSpaceElement(logicIt.getElement());
				
    			if(logicIt.getDistance()[0] < logicIt.getDistance()[1] ){
    				
    				entry.setRepresentativeDistance(logicIt.getDistance()[0]);
    				node0.addEntry(entry);
    				
    			}else{
    				
    				entry.setRepresentativeDistance(logicIt.getDistance()[1]);
    				node1.addEntry(entry);
    			}
    		}	
    	}
    	return dCount;
    }
    
    
    private void addEntriesMinOccupation(NodeTree node, List<DistanceIndex> indexDistances){
    	
    	int countIdx = 0;
    	
		while(logicEntries.get(indexDistances.get(countIdx).getIndex()).isMapped()){
			countIdx++;
		}
		
		int currObj = indexDistances.get(countIdx).getIndex();
		logicEntries.get(currObj).setMapped(true);
		
		EntryTree entryTree = node.createEntry();
		entryTree.setSpaceElement(logicEntries.get(currObj).getElement());
		
		entryTree.setRepresentativeDistance(indexDistances.get(countIdx).getDistance());

		setEntryProperties(entryTree, currObj);
		
		node.addEntry(entryTree);
    	
    }
    
    public void setEntryProperties(EntryTree entryTree, int idx){
    	
		if(entryTree instanceof LeafEntryTree){
			
			LeafEntryTree leafEntryTree = (LeafEntryTree) entryTree;
			
			leafEntryTree.setRowId(logicEntries.get(idx).getRowId());
		
		}else{
			
			IndexEntryTree indexEntry = (IndexEntryTree) entryTree;
			LogicEntry logicEntry = logicEntries.get(idx);
			
			indexEntry.setConveringRadius(logicEntry.getRadius());
			indexEntry.setnEntries(logicEntry.getnEntries());
			indexEntry.setPtr(logicEntry.getBlockId());
		}
    }
    
    public int updateDistances(MetricEvaluator metricEvaluator){
    	
    	for(int i = 0; i < count; i++){
    		
    		if(i == repIndex[0]){

    			double [] distances = new double[]{ 0.0, Double.MAX_VALUE};
    			    			
    			logicEntries.get(i).setDistance(distances);
    		}
    		else if (i == repIndex[1]) {
    			
    			double [] distances = new double[]{ Double.MAX_VALUE, 0.0};
    			
    			logicEntries.get(i).setDistance(distances);
    	
    		}
    		else {

    			double distance0 =  metricEvaluator.getDistance(
    					logicEntries.get(repIndex[0]).getElement(),
    					logicEntries.get(i).getElement());
    		
    			double distance1 = metricEvaluator.getDistance(
    					logicEntries.get(repIndex[1]).getElement(),
    					logicEntries.get(i).getElement());
    			
    			logicEntries.get(i).setDistance(new double[]{distance0, distance1});
       		}
    	}
    	return (count * 2) - 2;
    }

	public NodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}
	
	public float[] getObject(int index){
		return logicEntries.get(index).getElement();
	}

	public String toString(){
		
		return "\n\nNÓ LÓGICO " +
				"\nEntradas => " +  logicEntries + 
				"\n Min Occup => " + minOccupation +
				"\n Max Entradas => " + maxEntries +
				"\n Count => " + count;
	}
	
}