package br.uel.cross.parallel.util;


public class LogicEntry {

	private float[] element;
	
	private long blockId;
	
	private int nEntries;
	
	private boolean mine;
	
	private double [] distance;

	private boolean mapped;

	private double radius;

	private long rowId;
	
	public LogicEntry(){
		distance = new double[2];
	}
	
	public float[] getElement() {
		return element;
	}

	public void setElement(float[] element) {
		this.element = element;
	}

	public long getBlockId() {
		return blockId;
	}

	public void setBlockId(long blockId) {
		this.blockId = blockId;
	}

	public boolean isMine() {
		return mine;
	}

	public void setMine(boolean mine) {
		this.mine = mine;
	}

	public double[] getDistance() {
		return distance;
	}

	public void setDistance(double[] distance) {
		this.distance = distance;
	}

	public boolean isMapped() {
		return mapped;
	}

	public void setMapped(boolean mapped) {
		this.mapped = mapped;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public int getnEntries() {
		return nEntries;
	}

	public void setnEntries(int nEntries) {
		this.nEntries = nEntries;
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}
	
	public String toString(){
		return "Elemento => "+ element;
	}

}