package br.uel.cross.parallel.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class NetworkConfUtil {

	public static List<String> getHostsFromHadoop(String path) {

		List<String> hostNames = new ArrayList<String>();

		try {
			FileInputStream fis = new FileInputStream(path);
			BufferedReader reader = new BufferedReader(new InputStreamReader(fis));

			String line = reader.readLine();

			while (line != null) {
				//Tirando os hosts comentados
				if (!(line.length() == 0) && line.charAt(0) != '#') {
					hostNames.add(line);
				}
				line = reader.readLine();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return hostNames;
	}


}
