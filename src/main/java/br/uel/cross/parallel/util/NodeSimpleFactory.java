package br.uel.cross.parallel.util;

import br.uel.cross.parallel.mams.model.IndexNodeTree;
import br.uel.cross.parallel.mams.model.LeafNodeTree;
import br.uel.cross.parallel.mams.model.NodeTree;

public class NodeSimpleFactory {

	
	public static NodeTree create(boolean isLeaf){
		
		if(isLeaf){
			return new LeafNodeTree();
		}
		else{
			return new IndexNodeTree();
		}
	}
}
