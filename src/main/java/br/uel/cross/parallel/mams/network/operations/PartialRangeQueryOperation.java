package br.uel.cross.parallel.mams.network.operations;

import br.uel.cross.parallel.mams.network.HostSocket;
import br.uel.cross.parallel.mams.server.ProtocolCode;

public class PartialRangeQueryOperation extends ParallelOperation{

	private double distance;
	
	public PartialRangeQueryOperation(HostSocket host, ParallelOperationCallback callback, long blockId, double distance){
		super(host, callback, blockId);
		this.distance = distance;
	}
	
	@Override
	public Object performOperation() {
		return host.partialRangeQuery(blockId, distance);
	}

	@Override
	public ProtocolCode getServerOperation() {
		return ProtocolCode.PARTIALRANGEQUERY;
	}

}
