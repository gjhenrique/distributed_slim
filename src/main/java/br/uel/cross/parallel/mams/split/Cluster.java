package br.uel.cross.parallel.mams.split;

public class Cluster {
	
	private ClusterState state;
	
	private int size;
	
	private double minDist;
	
	private int src;
	
	private int dst;

	public ClusterState getState() {
		return state;
	}

	public void setState(ClusterState state) {
		this.state = state;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public double getMinDist() {
		return minDist;
	}

	public void setMinDist(double minDist) {
		this.minDist = minDist;
	}

	public int getSrc() {
		return src;
	}

	public void setSrc(int src) {
		this.src = src;
	}

	public int getDst() {
		return dst;
	}

	public void setDst(int dst) {
		this.dst = dst;
	}
}
