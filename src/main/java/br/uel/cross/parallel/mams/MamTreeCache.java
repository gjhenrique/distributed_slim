package br.uel.cross.parallel.mams;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.pagemanager.PageManager;
import br.uel.cross.parallel.mams.result.ResultPair;
import br.uel.cross.parallel.mams.result.TreeResult;
import br.uel.cross.parallel.mams.split.Promoter;
import br.uel.cross.parallel.mams.subtreechooser.SubTreeChooser;

public class MamTreeCache extends MamTree {

    private int limitElements;
    private MamTree bigSlim;
    private int counter;

    public MamTreeCache(int limitElements, PageManager pageManager, MetricEvaluator metricEvaluator, Promoter promoter, SubTreeChooser subtreeChooser, MamTree bigSlim) {
        super(pageManager, metricEvaluator, promoter, subtreeChooser);
        this.limitElements = limitElements;
        this.bigSlim = bigSlim;
    }

    @Override
    public boolean add(float[] element, long rowId) {
        this.counter++;
        boolean split = super.add(element, rowId);

        if (counter > limitElements) {
            flushCache();
        }
        return split;
    }

    private void flushCache() {

        // Itera sobre todos os nós folha
        // Para cada nó folha, descobrir qual é o pivo (nó interno do nível h-1) na Slimzona
        // Append folha da Slimzinha na folha da Slimzona
        // Verificar se o pivo appendado não vai ser modificado por causa das novas adições
        // Se modificou, atualizar os indíces da Slimzona bottom-up
        // Verificar se a folha da Slimzona não excedeu tamanho da página da Slimzona

        // Delete cache uma vez que todos os elementos ja foram "exportados"    }
    }

    @Override
    public TreeResult simpleRangeQuery(float[] sample, double range) {

        // Main Index results
        TreeResult indexResult = bigSlim.simpleRangeQuery(sample, range);

        // Cache results
        TreeResult cacheResult = super.simpleRangeQuery(sample, range);

        indexResult.mergeResult(cacheResult);

        return indexResult;
    }

    @Override
    public void deleteIndex() {
        counter = 0;
        super.deleteIndex();
    }

    @Override
    public TreeResult nearestQuery(float[] sample, int k, boolean tie) {
        // Main Index results
        TreeResult indexResult = bigSlim.nearestQuery(sample, k, tie);

        // Cache results
        TreeResult cacheResult = super.nearestQuery(sample, k, tie);

        for (ResultPair pairs : cacheResult.getPairs()) {
            indexResult.addPair(pairs);
            indexResult.knnCutOldResult();
        }

        return indexResult;

    }
}
