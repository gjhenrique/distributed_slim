package br.uel.cross.parallel.mams.network;

import java.util.List;

import br.uel.cross.parallel.mams.result.TreeResult;
import br.uel.cross.parallel.mams.statiscs.QueryStats;

public interface QueryResultCallback {
	
	public void handleQueryResult(TreeResult result, String identifier, List<QueryStats> queryStats);
}
