package br.uel.cross.parallel.mams.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.pagemanager.PageManager;

public class RequestDispacher implements Runnable{

	private Socket socket;
	private PageManager pageManager;
	private MetricEvaluator metricEvaluator;
	
	public RequestDispacher(Socket socket, PageManager pageManager, MetricEvaluator metricEvaluator){
		this.socket = socket;
		this.pageManager = pageManager;
		this.metricEvaluator = metricEvaluator;
	}
	
	@Override
	public void run() {
		

		try {
			
			System.out.println("Nova requisição para o servidor");
			
			BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.flush();
			BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
			ObjectInputStream ois = new ObjectInputStream(bis);
			
			int protocolId = ois.readInt();
			ProtocolCode code = ProtocolCode.getCode(protocolId);
			System.out.println("Requisição de código " + code);
			ServerHandler handler = ServerHandlerFactory.create(code, pageManager, metricEvaluator);
			
			handler.handle(ois, oos);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
}
