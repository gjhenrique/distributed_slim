package br.uel.cross.parallel.mams.result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public class TreeResult extends Result implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -834113571681294946L;

	protected float[] sample;

	// Implentação da RedBlack
	protected TreeMap<ResultPair, ResultPair> pairs = new TreeMap<ResultPair, ResultPair>(
			new CompareKeysDistance());

	public void setQueryInfo(float[] sample, QueryType queryType, int k, double radius,
			double innerRadius, boolean tie) {
		this.sample = sample;
		this.queryType = queryType;
		this.k = k;
		this.radius = radius;
		this.innerRadius = innerRadius;
		this.tie = tie;
	}

	// Range Query
	public void setQueryInfoRangeQuery(float[] sample, double radius) {
		this.setQueryInfo(sample, QueryType.RANGEQUERY, -1, radius, 0.0, false);
	}

	// kNN
	public void setQueryInfoKnn(float[] sample, int k, double innerRadius, boolean tie) {
		this.setQueryInfo(sample, QueryType.KNEARESTQUERY, k, 0.0, innerRadius, tie);
	}

	public ResultPair getPair(int idx) {

		int i = 0;
		for (Map.Entry<ResultPair, ResultPair> entry : pairs.entrySet()) {
			if (i == idx) {
				return entry.getValue();
			}
			i++;
		}
		return null;
	}

	public void addPair(long rowId, double distance) {

		ResultPair result = new ResultPair(rowId, distance);

		pairs.put(result, result);

	}

	public float[] getSample() {
		return this.sample;
	}

	@Override
	public Collection<ResultPair> getPairs() {
		return new ArrayList<ResultPair>(pairs.values());
	}

	public String toString() {

		String s = "Resultado ";
		for (Map.Entry<ResultPair, ResultPair> entry : pairs.entrySet()) {
			ResultPair pair = entry.getValue();

			long element = pair.getRowId();

			String strReturn = "\n";
			strReturn += "\n" + element;
			
			s += strReturn + "\t" + pair.getDistance();
		}
		return s;
	}

	@Override
	public void mergeResult(Result result) {
		for (ResultPair resultPair : result.getPairs()) {
			this.pairs.put(resultPair, resultPair);
		}
	}

	public int getNumberOfEntries() {
		return this.pairs.size();
	}

	public void cut(int limit) {
		
		int difference = this.pairs.size() - limit;
		
		for(int i = 0; i < difference; i++){
			this.pairs.pollLastEntry();
		}
		
	}

	public double getMaximumDistance() {
		return this.pairs.lastKey().getDistance();
	}

}
