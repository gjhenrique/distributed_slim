package br.uel.cross.parallel.mams.network.operations;

import br.uel.cross.parallel.mams.network.HostSocket;
import br.uel.cross.parallel.mams.server.ProtocolCode;

public abstract class ParallelOperation implements Runnable{
	
	protected HostSocket host;
	
	protected ParallelOperationCallback callback;
	
	protected long blockId;
	
	public ParallelOperation(HostSocket host, ParallelOperationCallback callback, long blockId){
		this.host = host;
		this.callback = callback;
		this.blockId = blockId;
	}

	@Override
	public void run() {
		Object returnObject = this.performOperation();

		if (returnObject != null)
			this.callback.onSuccess(this.getServerOperation(), this.host, returnObject, blockId);
	}
	
	public abstract Object performOperation();

	public abstract ProtocolCode getServerOperation();
}
