package br.uel.cross.parallel.mams.split;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.util.LogicNode;

public class MstSplitterPromoter implements Promoter {

	private float[][] distanceMatrix;

	private int N;

	private Cluster[] clusters;

	private int[] objectCluster;

	private int cluster0;

	private int cluster1;

	private LogicNode logicNode;

	@Override
	public void promote(LogicNode logicNode, MetricEvaluator metricEvaluator) {

		this.logicNode = logicNode;

		initializePromoter();

		buildDistanceMatrix(metricEvaluator, logicNode);

		performMst();

	}
	
	private void initializePromoter() {

		N = logicNode.getCount();

		distanceMatrix = new float[N][N];

		clusters = new Cluster[N];

		for (int i = 0; i < clusters.length; i++) {
			clusters[i] = new Cluster();
		}

		objectCluster = new int[N];
	}
	
	private int buildDistanceMatrix(MetricEvaluator metricEvaluator, LogicNode node) {

		for (int i = 0; i < N; i++) {
			distanceMatrix[i][i] = 0;

			for (int j = 0; j < N; j++) {

				distanceMatrix[i][j] = (float) metricEvaluator.getDistance(node.getObject(i), node.getObject(j));

				distanceMatrix[j][i] = distanceMatrix[i][j];
			}
		}

		return ((1 - N) * N) / 2;
	}

	private void performMst() {
		int cc, iBig, iBigOpposite, a, b, c;
		boolean linksOk, flag;
		double big;

		// Insert each object in its own cluster.
		cc = N;
		for (int i = 0; i < N; i++) {
			clusters[i].setSize(1);
			clusters[i].setState(ClusterState.ALIVE);
			objectCluster[i] = i; // Add Object

		}// end for

		// Perform it until it reaches 2 clusters.
		while (cc > 2) {
			// Find the minimum distance between a cluster and its nearest
			// neighbour (connections).
			for (int i = 0; i < N; i++) {
				if (clusters[i].getState() != ClusterState.DEAD) {
					clusters[i].setMinDist(Double.MAX_VALUE);
				}// end if
			}// end for

			for (int i = 0; i < N; i++) {
				int k = objectCluster[i];
				// Locate the nearest
				for (int j = 0; j < N; j++) {
					if (objectCluster[j] != k) {
						if (clusters[k].getMinDist() > distanceMatrix[i][j]) {
							clusters[k].setMinDist(distanceMatrix[i][j]);
							clusters[k].setSrc(i);
							clusters[k].setDst(j);
						}// end if
					}// end if
				}// end for
			}// end for
			linksOk = true;

			// Find the largest connection. It will also locate the oposite
			// objects
			big = -1.0;
			iBig = 0;
			for (int i = 1; i < N; i++) {
				if ((clusters[i].getState() != ClusterState.DEAD) && (big < clusters[i].getMinDist())) {
					big = clusters[i].getMinDist();
					iBig = i;
				}// end if
			}// end for

			// Locate the iBigOpposite.
			iBigOpposite = iBig;
			for (int i = 0; i < N; i++) {
				if ((clusters[i].getState() != ClusterState.DEAD)
						&& (clusters[i].getSrc() == clusters[iBig].getDst())
						&& (clusters[i].getDst() == clusters[iBig].getSrc())) {
					iBigOpposite = i;
				}// end if
			}// end for

			// Join clusters
			int i = 0;
			while ((i < N) && (cc > 2)) {
				if ((i != iBig) && (i != iBigOpposite) && (clusters[i].getState() != ClusterState.DEAD)) {
					// Join cluster i and its nearest cluster.
					int k = objectCluster[clusters[i].getDst()];
					flag = true;

					// Change the cluster of all objects of the dropped one to
					// the remaining one.
					for (int j = 0; j < N; j++) {
						if ((objectCluster[j] == k)
								&& (objectCluster[j] != objectCluster[clusters[i].getSrc()])) {
							if ((cc == 3) && (flag)) {
								if (!linksOk) {
									// Force update.
									i = j = N;
								} else {
									a = objectCluster[clusters[iBig].getSrc()];
									b = objectCluster[clusters[iBig].getDst()];
									c = k;
									if ((c == a) || (c == b)) {
										c = objectCluster[clusters[i].getSrc()];
									}// end if

									if ((objectCluster[clusters[c].getSrc()] == a)
											|| (objectCluster[clusters[c].getDst()] == a)) {
										if (clusters[b].getSize() > clusters[c].getSize()) {
											// Join C and A
											joinClusters(c, a);
										} else {
											// Join B and A
											joinClusters(b, a);
										}// end if
									} else {
										if (clusters[a].getSize() > clusters[c].getSize()) {
											// Join C and B
											joinClusters(c, b);
										} else {
											// Join A and B
											joinClusters(a, b);
										}// end if
									}// end if
									i = j = N;
									cc--;
								}// end if
							} else {
								clusters[k].setState(ClusterState.DEATH_SENTENCE);
								objectCluster[j] = objectCluster[clusters[i].getSrc()];
								if (flag) {

									Cluster srcCluster = clusters[objectCluster[clusters[i].getSrc()]];

									srcCluster.setSize(srcCluster.getSize() + clusters[k].getSize());

									cc--;
									flag = false;
									linksOk = false;
								}// end if
							}// end if
						}// end if
					}// end for
				}// end if
					// Update i for the next loop
				i++;
			}// end while

			// All clusters that are destiny of an edge has gone, integrated
			// into another one.
			for (i = 0; i < N; i++) {
				if (clusters[i].getState() == ClusterState.DEATH_SENTENCE) {
					clusters[i].setState(ClusterState.DEAD);
				}// end if
			}// end for
		}// end while

		// Locate the name of the 2 clusters.
		cluster0 = -1;
		cluster1 = -1;
		for (int i = 0; i < N; i++) {
			if (clusters[i].getState() == ClusterState.ALIVE) {
				if (cluster0 == -1) {
					cluster0 = i;
				} else if (cluster1 == -1) {
					cluster1 = i;
				}// end if
			}// end if
		}// end for

		for (int i = 0; i < N; i++) {
			if ((objectCluster[i] != cluster0) && (objectCluster[i] != cluster1)) {
				throw new RuntimeException("At least on object has no cluster.");
			}// end if
		}// end for

		logicNode.setRepresentatives(findCenter(cluster0), findCenter(cluster1));
		
	}

	private int findCenter(int cluster) {
		int center = 0;
		double minRadius, radius;

		minRadius = Double.MAX_VALUE;
		for (int i = 0; i < N; i++) {
			if (objectCluster[i] == cluster) {
				radius = -1;
				for (int j = 0; j < N; j++) {
					if ((objectCluster[j] == cluster) && (radius < distanceMatrix[i][j])) {
						radius = distanceMatrix[i][j];
					}// end if
				}// end for
				if (minRadius > radius) {
					minRadius = radius;
					center = i;
				}// end if
			}// end if
		}// end for

		return center;
	}

	private void joinClusters(int cluster1, int cluster2) {

		for (int i = 0; i < N; i++) {
			if (objectCluster[i] == cluster2) {
				objectCluster[i] = cluster1;
			}// end if
		}// end for

		Cluster clusterObject1 = clusters[cluster1];
		Cluster clusterObject2 = clusters[cluster2];

		clusterObject1.setSize(clusterObject1.getSize() + clusterObject2.getSize());
		clusters[cluster2].setState(ClusterState.DEATH_SENTENCE);
	}

	@Override
	public void distribute(LogicNode logicNode, NodeTree node0, float[] rep0, NodeTree node1,
			float[] rep1, MetricEvaluator metricEvaluator) {

		this.addInitialEntryToNode(node0, 0);
		this.addInitialEntryToNode(node1, 1);

		for (int i = 0; i < N; i++) {

			if (!this.logicNode.isRepresentative(i)) {
				if (objectCluster[i] == cluster0) {

					addEntryToNode(node0, node1, i, 0, 1);

				} else {

					addEntryToNode(node1, node0, i, 1, 0);
				}
			}
		}
		
		if(logicNode.getLogicEntries().size() != (node0.getNumberOfEntries() + node1.getNumberOfEntries())){
			throw new RuntimeException("Tamanho dos dos dois nós repartidos é diferente do nó original");
		}
		
		this.distanceMatrix = null;
		this.clusters = null;
		this.objectCluster = null;
	}

	private void addEntryToNode(NodeTree chosenNode, NodeTree alternativeNode, int index, int chosenPosition,
			int alternativePosition) {

		EntryTree entry = chosenNode.createEntry();
		entry.setSpaceElement(this.logicNode.getObject(index));

		boolean fit = chosenNode.addEntry(entry);
		
		if (fit) {

			entry.setRepresentativeDistance(distanceMatrix[index][logicNode
					.getRepresentativeIndex(chosenPosition)]);

		} else {
			
			alternativeNode.addEntry(entry);
			entry.setRepresentativeDistance(distanceMatrix[index][logicNode
					.getRepresentativeIndex(alternativePosition)]);
		}

		logicNode.setEntryProperties(entry, index);

	}

	private void addInitialEntryToNode(NodeTree node, int index) {

		EntryTree entry = node.createEntry();
		
		int objIndex = this.logicNode.getRepresentativeIndex(index);
		
		float[] element = this.logicNode.getRepresentativeElement(index);
		
		entry.setSpaceElement(element);
		entry.setRepresentativeDistance(0.0);

		logicNode.setEntryProperties(entry, objIndex);

		node.addEntry(entry);
	}
}
