package br.uel.cross.parallel.mams.split;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.util.LogicNode;

public interface Promoter {

	public void promote(LogicNode logicNode, MetricEvaluator metricEvaluator);

	public void distribute(LogicNode logicNode, NodeTree oldNode, float[] lRep, NodeTree newNode,
			float[] rRep, MetricEvaluator metricEvaluator);

}
