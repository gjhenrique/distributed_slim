package br.uel.cross.parallel.mams.model;


public class LeafEntryTree extends EntryTree {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6306331968415712574L;
	
	private long rowId;
	
	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}
	
}
