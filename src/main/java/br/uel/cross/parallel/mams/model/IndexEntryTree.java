package br.uel.cross.parallel.mams.model;


public class IndexEntryTree extends EntryTree{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7922819669714572834L;

	private double converingRadius;
	
	private long ptr;

	private int nEntries;
	
	public IndexEntryTree(){
		
	}
	
	public IndexEntryTree(int nEntries, long ptr, double coveringRadius, float[] elements, double representativeDistance){
		
		super(elements, representativeDistance);
		this.ptr = ptr;
		this.converingRadius = coveringRadius;
		this.nEntries = nEntries;
	}
	
	public double getConveringRadius() {
		return converingRadius;
	}

	public void setConveringRadius(double converingRadius) {
		this.converingRadius = converingRadius;
	}

	public long getPtr() {
		return ptr;
	}

	public void setPtr(long ptr) {
		this.ptr = ptr;
	}

	public int getnEntries() {
		return nEntries;
	}

	public void setnEntries(int nEntries) {
		this.nEntries = nEntries;
	}
	
	
}
