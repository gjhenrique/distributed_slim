package br.uel.cross.parallel.mams.pagemanager;

import br.uel.cross.parallel.mams.model.NodeTree;

import java.util.HashMap;
import java.util.Map;

public class MemoryPageManager implements PageManager{

    private Map<Long, NodeTree> nodeMap = new HashMap<>();

    private NodeTree root;

    private int height;

    private long usedPages;

    @Override
    public NodeTree findRoot() {
        return root;
    }

    @Override
    public long getBlockId() {
        return usedPages++;
    }

    @Override
    public void writeNode(long blockId, NodeTree nodeTree) {
        nodeMap.put(blockId, nodeTree);
    }

    @Override
    public void changeRootNode(long blockId, int height) {
        root = nodeMap.get(blockId);
        this.height = height;
    }

    @Override
    public NodeTree fromFileToNode(long blockId) {
        return nodeMap.get(blockId);
    }

    @Override
    public int getTreeHeight() {
        return height;
    }

    @Override
    public boolean pageExists(long blockId) {
        return nodeMap.containsKey(blockId);
    }

    @Override
    public void deleteIndex() {
        this.nodeMap.clear();
    }
}
