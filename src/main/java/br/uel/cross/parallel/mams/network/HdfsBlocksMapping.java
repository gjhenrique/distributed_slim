package br.uel.cross.parallel.mams.network;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class HdfsBlocksMapping {

	public static Map<Long, String[]> getBlocksLocations(FileSystem fs, String path) {

		Map<Long, String[]> blocksLocations = new ConcurrentHashMap<Long, String[]>();

		try {
			FileStatus[] stats = fs.listStatus(new Path(path));

			if (stats == null) {

				throw new RuntimeException("Relacao de blocos/hosts é nula");
			}
			for (FileStatus stat : stats) {
				String pathName = stat.getPath().toString();
				System.out.println(pathName);
				String[] names = pathName.split("/");
				String blockString = names[names.length - 1];

				try {

					long blockIdentifier = Long.parseLong(blockString);

					// Tenta achar o offset de um byte, pois a ideia inicial é
					// do bloco não passar o tamanho padrão do bloco do HDFS
					BlockLocation[] hostsBlockLocation = fs.getFileBlockLocations(stat, 0, 1);
					String[] hosts = hostsBlockLocation[0].getHosts();

					blocksLocations.put(blockIdentifier, hosts);
				} catch (NumberFormatException nfe) {
					// Nao faz nada pq esta tentando instanciar a raiz que é uma
					// string
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return blocksLocations;
	}
	
	public static Map<Long, String[]> getBlocksLocal(File mamDir, String []hosts){
	
		File []files = mamDir.listFiles();
		
		Map<Long, String[]> blocksLocations = new ConcurrentHashMap<Long, String[]>();
		
		for(File file : files){
			
			try {

				long blockIdentifier = Long.parseLong(file.getName());
				
				blocksLocations.put(blockIdentifier, hosts);
			} catch (NumberFormatException nfe) {
			}
			
		}
		
		return blocksLocations;
	}
}
