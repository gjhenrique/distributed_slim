package br.uel.cross.parallel.mams.network;

public interface ParallelService {

	public void startService();
	public void stopService();
}