package br.uel.cross.parallel.mams.server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public interface ServerHandler {

	public void handle(ObjectInputStream ois, ObjectOutputStream oos);
	
}
