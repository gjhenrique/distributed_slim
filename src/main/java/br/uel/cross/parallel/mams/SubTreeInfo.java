package br.uel.cross.parallel.mams;


public class SubTreeInfo {
		
	private float[] rep;
	
	private double radius;
	
	private long rootId;
	
	private int nObjects;
	
	public float[] getRep() {
		return rep;
	}

	public void setRep(float[] rep) {
		this.rep = rep;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public long getRootId() {
		return rootId;
	}

	public void setRootId(long rootId) {
		this.rootId = rootId;
	}

	public int getnObjects() {
		return nObjects;
	}

	public void setnObjects(int nObjects) {
		this.nObjects = nObjects;
	}

	public String toString(){
		return "Representante ==> " + rep +
				"\nRaio ==> " + radius +
				"\n Ponteiro " + rootId +
				"\n Número de Objetos " + nObjects;
	}
	
}