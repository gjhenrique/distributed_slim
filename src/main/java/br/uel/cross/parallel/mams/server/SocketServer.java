package br.uel.cross.parallel.mams.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import br.uel.cross.parallel.mams.distance.EuclideanMetricEvaluator;
import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.pagemanager.PageManager;

public class SocketServer{

	private PageManager pageManager;

	private int portNumber;

	private MetricEvaluator metricEvaluator;
	
	private ExecutorService threadPool;
			
	public SocketServer(PageManager pageManager, int port, int numberOfThreads){
		this.pageManager = pageManager;
		this.portNumber = port;

		this.threadPool = Executors.newFixedThreadPool(numberOfThreads);
		//Passar pelo construtor?
		metricEvaluator = new EuclideanMetricEvaluator();
		
	}

	public void acceptNewConnections(){

		try {
			ServerSocket serverSocket = new ServerSocket(portNumber);

			while(true){

				System.out.println("Esperando novas conexões");

				Socket socket = serverSocket.accept();
				
				RequestDispacher rd = new RequestDispacher(socket, pageManager,metricEvaluator);
				
				this.threadPool.submit(rd);
			}	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
