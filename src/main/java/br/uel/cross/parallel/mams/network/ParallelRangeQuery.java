package br.uel.cross.parallel.mams.network;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import br.uel.cross.parallel.mams.network.operations.InitialRangeQueryOperation;
import br.uel.cross.parallel.mams.network.operations.ParallelOperation;
import br.uel.cross.parallel.mams.network.operations.ParallelOperationCallback;
import br.uel.cross.parallel.mams.network.operations.PartialRangeQueryOperation;
import br.uel.cross.parallel.mams.result.Result;
import br.uel.cross.parallel.mams.result.TreeResult;
import br.uel.cross.parallel.mams.server.ProtocolCode;
import br.uel.cross.parallel.mams.statiscs.QueryStats;

public class ParallelRangeQuery implements ParallelOperationCallback{

	private List<HostSocket> hosts;

	private ParallelService blocksDispatcher = new BlockDispatcherService(this);

	private volatile ConcurrentMap<Long, Double> blocksToVisit = new ConcurrentHashMap<Long, Double>();

	private volatile ConcurrentMap<Long, String> blocksBusy = new ConcurrentHashMap<Long, String>();

	private Map<Long, String[]> locationBlocks;

	private TreeResult treeResult = new TreeResult();

	private String identifier;

	private QueryResultCallback resultCallback;

	public ParallelRangeQuery(List<HostSocket> hosts, Map<Long, String[]> locationBlocks, String identifier, QueryResultCallback resultCallback) {

		this.hosts = hosts;

		this.locationBlocks = locationBlocks;

		this.identifier = identifier;

		this.resultCallback = resultCallback;
	}

	public synchronized void initializeRangeQueryConnections(float[] sample, double range) {

		System.out.println("Inicializando Hosts");
		initializeHostsConnections();

		System.out.println("Enviando header inicial para os servidores");
		for (HostSocket host : hosts) {
			System.out.println("Enviando header inicial para " + host);
			if (host.isActive()) {
				//				host.initializeRangeQuery(sample, range);
				// TODO Verificar se envia em outra thread
				ParallelOperation initializeOperation = new InitialRangeQueryOperation(host, this, sample,
						range);
				new Thread(initializeOperation).start();
			}
		}
	}

	private void initializeHostsConnections() {

		for (HostSocket host : hosts) {
			host.connectServer();
		}
		blocksDispatcher.startService();
	}

	public void parallelRangeQuery(long blockId, double representativeDist) {

		System.out.println("Nova requisicao RQ => " + blockId);

		blocksToVisit.putIfAbsent(blockId, representativeDist);

		this.dispatchBlocksToHosts();
	}

	@Override
	public synchronized void dispatchBlocksToHosts() {

		// Iterando sobre o bloco
		for (Map.Entry<Long, Double> entry : blocksToVisit.entrySet()) {

			long blockToVisit = entry.getKey();

			// Verficando se o bloco não está sendo processado por algum
			// host
			List<HostSocket> classifiedHosts = findHostsHavingBlock(blockToVisit);

			Random random = new Random();
			int randomNumber = random.nextInt(classifiedHosts.size());

			HostSocket host = classifiedHosts.get(randomNumber);

			if (host.isActive() && blocksBusy.get(blockToVisit) == null) {
				blocksBusy.putIfAbsent(blockToVisit, host.getHostName());

				ParallelOperation rangeQueryOperation = new PartialRangeQueryOperation(host, this,
						blockToVisit, entry.getValue());

				System.out.println(identifier + " => " + blockToVisit + " para a máquina " + host);

				new Thread(rangeQueryOperation).start();
			}	
		}
	}

	private List<HostSocket> findHostsHavingBlock(long blockId) {

		String[] blockLocations = locationBlocks.get(blockId);

		List<HostSocket> classifiedHosts = new ArrayList<HostSocket>(hosts.size());

		locationBlocks.get(blockId);
		for (HostSocket host : hosts) {

			if (blockLocations == null) {
				throw new RuntimeException("Nenhum host associado ao bloco " + blockId);
			}

			for (String hostBlock : blockLocations) {
				if (host.getHostName().equals(hostBlock)) {
					classifiedHosts.add(host);
				}
			}
		}
		return classifiedHosts;
	}


	private synchronized boolean checkForEndOfAlgorithm() {
		if (blocksToVisit.size() == 0 && !anyBusyHosts()) {
			return true;
		}
		return false;
	}

	private synchronized boolean anyBusyHosts() {
		for (HostSocket host : hosts) {
			if (host.isBusy()) {
				return true;
			}
		}
		return false;
	}

	private void closeAllHostConnections(){
		for(HostSocket host : this.hosts){
			host.closeConnection();
		}
	}

	private List<QueryStats> askForStatsHosts(){

		List<QueryStats> listStats = new ArrayList<QueryStats>();
		for(HostSocket host : this.hosts){
			if(host.isActive()){
				QueryStats stats = host.askForStats();
				listStats.add(stats);
			}
		}
		return listStats;
	}

	@Override
	public synchronized void onSuccess(ProtocolCode code, HostSocket host, Object resultObject, long blockId) {

		blocksBusy.remove(blockId);
		blocksToVisit.remove(blockId);

		if (resultObject instanceof Result) {
			//System.out.println("Recebendo como resultado o TreeResult");

			System.out.println("Tamanho dos blocos ocupados " + blocksBusy.size());
			Result result = (Result) resultObject;
			this.treeResult.mergeResult(result);

			if (checkForEndOfAlgorithm()) {
				List<QueryStats> listStats = this.askForStatsHosts();
				this.blocksDispatcher.stopService();
				System.out.println("Fechando as conexões");
				this.closeAllHostConnections();	
				this.resultCallback.handleQueryResult(this.treeResult, this.identifier, listStats);
			}

		} else if (resultObject instanceof Map) {

			//System.out.println("Recebendo como resultado mais ids");

			@SuppressWarnings("unchecked")
			Map<Long, Double> mapResponse = (Map<Long, Double>) resultObject;

			// Adicionando objetos no mapa
			for (Map.Entry<Long, Double> entry : mapResponse.entrySet()) {

				System.out.println("Recebido ID => " + entry.getKey());
				this.parallelRangeQuery(entry.getKey(), entry.getValue());
			}
		} else if (resultObject instanceof Integer) {
			System.out.println("Tamanho dos blocos ocupados " + blocksBusy.size());
			System.out.println(resultObject);
			if (checkForEndOfAlgorithm()) {

				List<QueryStats> listStats = this.askForStatsHosts();
				this.blocksDispatcher.stopService();
				System.out.println("Fechando as conexões");
				this.closeAllHostConnections();	
				this.resultCallback.handleQueryResult(this.treeResult, this.identifier, listStats);
			}
			
		} else {
			throw new RuntimeException("Resposta do Servidor veio em formato diferente do esperado");
		}
	}

	@Override
	public void onError(HostSocket host) {
		// Tornar o host inativo
	}

}
