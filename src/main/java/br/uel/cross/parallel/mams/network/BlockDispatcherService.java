package br.uel.cross.parallel.mams.network;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import br.uel.cross.parallel.mams.network.operations.ParallelOperationCallback;

public class BlockDispatcherService implements ParallelService {

	private ParallelOperationCallback operationCallback;
	
	private final static int MILLISECONDS = 10000;
	
	ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
	
	public BlockDispatcherService(ParallelOperationCallback operationCallback) {
		this.operationCallback = operationCallback;
	}

	@Override
	public void startService() {

		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				BlockDispatcherService.this.operationCallback.dispatchBlocksToHosts();
			}
		}, 5000, MILLISECONDS, TimeUnit.MILLISECONDS);

	}

	@Override
	public void stopService() {
		exec.shutdown();
	}

}
