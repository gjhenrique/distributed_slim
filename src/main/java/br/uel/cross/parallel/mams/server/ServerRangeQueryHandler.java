package br.uel.cross.parallel.mams.server;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.model.IndexEntryTree;
import br.uel.cross.parallel.mams.model.IndexNodeTree;
import br.uel.cross.parallel.mams.model.LeafEntryTree;
import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.mams.pagemanager.PageManager;
import br.uel.cross.parallel.mams.result.TreeResult;
import br.uel.cross.parallel.mams.statiscs.QueryStats;
import br.uel.cross.parallel.util.TimeFormatter;

public class ServerRangeQueryHandler implements ServerHandler {

	private PageManager pageManager;
	private MetricEvaluator metricEvaluator;

	public QueryStats stats;

	private Object lock;

	public ServerRangeQueryHandler(PageManager pageManager, MetricEvaluator metricEvaluator) {
		this.pageManager = pageManager;
		this.metricEvaluator = metricEvaluator;
	}

	@Override
	public void handle(ObjectInputStream ois, ObjectOutputStream oos) {

		try {

			int elementSize = ois.readInt();
			float[] sample = new float[elementSize];

			for (int i = 0; i < elementSize; i++) {
				sample[i] = ois.readFloat();
			}

			double range = ois.readDouble();

			System.out.println("Nova requisição  " + sample);

			while (true) {

				int protocolId = ois.readInt();
				ProtocolCode code = ProtocolCode.getCode(protocolId);

				if (code == ProtocolCode.CLOSECONNECTION) {
					this.closeConnection(ois, oos);
					break;
				}

				else if (code == ProtocolCode.ASK_FOR_STATS) {
					System.out.println("Enviando estatisticas");
					oos.writeObject(getQueryStats());
					oos.flush();
				} else {

					long blockId = ois.readLong();
					double distance = ois.readDouble();
					
					// Parte demorada que ocorre a transferência do bloco
					NodeTree node = pageManager.fromFileToNode(blockId);
					
					Object result = rangeQuery(node, sample, range, distance);
//					
//					oos.writeInt(0);
					/*if (result instanceof TreeResult) {
						TreeResult treeResult = (TreeResult) result;
						if (treeResult.getNumberOfEntries() == 0) {
							oos.writeObject("VAZIO");
						} else {
							oos.writeObject(result);
						}
					}*/
					oos.writeObject(result);
					oos.flush();
					// Liberando o nó para o coletor de lixo
					node = null;
				}
			}
		} catch (EOFException e) {
			this.closeConnection(ois, oos);
			e.printStackTrace();
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void closeConnection(ObjectInputStream ois, ObjectOutputStream oos){
		System.out.println("Requisicao terminada");
		try {
			oos.close();
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private QueryStats getQueryStats() {
		return stats;
	}

	public Object rangeQuery(NodeTree node, float[] sample, double range, double representativeDistance) {

		double distance;
		if (node instanceof IndexNodeTree) {

			Map<Long, Double> mapResult = new HashMap<Long, Double>();
			for (EntryTree entry : node.getEntry()) {
				IndexEntryTree indexEntry = (IndexEntryTree) entry;
				if (Math.abs(representativeDistance - indexEntry.getRepresentativeDistance()) <= range
						+ indexEntry.getConveringRadius()) {
					distance = this.metricEvaluator.getDistance(indexEntry.getSpaceElement(), sample);

					if (distance <= range + indexEntry.getConveringRadius()) {
						mapResult.put(indexEntry.getPtr(), distance);
					}
				}
			}
			return mapResult;
		} else {

			TreeResult treeResult = new TreeResult();

			for (EntryTree entry : node.getEntry()) {

				if (Math.abs(representativeDistance - entry.getRepresentativeDistance()) <= range) {

					distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);

					LeafEntryTree leafEntry = (LeafEntryTree) entry;

					if (distance <= range) {
						treeResult.addPair(leafEntry.getRowId(), distance);
					}
				}
			}
			return treeResult;
		}
	}

}
