package br.uel.cross.parallel.mams.statiscs;

import java.io.Serializable;

import br.uel.cross.parallel.util.TimeFormatter;

public enum StatsType implements Serializable{
	
	DISTANCE_COMPUTATION("Computação de distâncias", StatsAttributeType.COUNTER),
	DISK_ACCESS_READING("Número de acessos a disco - Leitura", StatsAttributeType.COUNTER), 
	AVG_DISK_ELEMENTS_READING("Média de elementos retornados - Leitura", StatsAttributeType.AVERAGE), 
	AVG_TIME_ACCESS_DISK_READING("Média do tempo de acesso ao disco - Leitura", StatsAttributeType.AVERAGE, true),
	DISK_ACCESS_WRITING("Número de acessos a disco - Escrita", StatsAttributeType.COUNTER),
	AVG_DISK_ELEMENTS_WRITING("Média de elementos retornados - Escrita", StatsAttributeType.AVERAGE), 
	AVG_TIME_ACCESS_DISK_WRITING("Média do tempo de acesso ao disco - Escrita", StatsAttributeType.AVERAGE, true),
	AVG_TIME_READ_OBJECT("Média do tempo para leitura do objeto", StatsAttributeType.AVERAGE, true),
	MAX_ENTRIES_INDEX_NODE("Máximo de entradas nó índice", StatsAttributeType.AVERAGE),
	MAX_ENTRIES_LEAF_NODE("Máximo de entradas nó folha", StatsAttributeType.AVERAGE),
	CACHE_HIT("Cache hit", StatsAttributeType.COUNTER), 
	CACHE_MISS("Cache miss", StatsAttributeType.COUNTER);

	private String name;
	private boolean isTime;
	private StatsAttributeType type;
	
	private StatsType(String name, StatsAttributeType type) {
		this.type = type;
		this.name = name;
	}

	private StatsType(String name, StatsAttributeType type, boolean isTime){
		this(name, type);
		this.isTime = isTime;
	}
	
	public StatsAttributeType getType(){
		return this.type;
	}
	
	public String writeStats(int value){

		String valueString = (isTime) ? 
				TimeFormatter.formattedTime(value)+"" :
					value+"";

		return this.name + " => " + valueString;
	}

	@Override
	public String toString() {
		return name;
	}
}
