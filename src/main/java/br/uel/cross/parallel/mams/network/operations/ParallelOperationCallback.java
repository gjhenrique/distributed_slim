package br.uel.cross.parallel.mams.network.operations;

import br.uel.cross.parallel.mams.network.HostSocket;
import br.uel.cross.parallel.mams.server.ProtocolCode;

public interface ParallelOperationCallback {

	public void onSuccess(ProtocolCode code, HostSocket host, Object object, long blockId);
	public void dispatchBlocksToHosts();
	public void onError(HostSocket host);
	
}
