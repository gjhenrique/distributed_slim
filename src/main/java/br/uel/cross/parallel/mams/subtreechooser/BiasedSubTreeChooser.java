package br.uel.cross.parallel.mams.subtreechooser;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.IndexEntryTree;
import br.uel.cross.parallel.mams.model.IndexNodeTree;

public class BiasedSubTreeChooser implements SubTreeChooser{

	@Override
	public int subtreeChooser(IndexNodeTree node, float[] element, MetricEvaluator metricEvaluator) {
		int idx = 0, minIndex = 0;
		
		double minDistance = Double.MAX_VALUE;
				
		boolean stop = ( idx >= node.getNumberOfEntries() );
		
		while(!stop){
			
			IndexEntryTree entry = (IndexEntryTree) node.getEntry().get(idx);
			
			double distance = metricEvaluator.getDistance(element, entry.getSpaceElement());
			
			if(distance < entry.getConveringRadius()){
				stop = true;
				minDistance = 0;
				minIndex = idx;
			}
			else if( distance - entry.getConveringRadius() < minDistance){
				
				minDistance = distance - entry.getConveringRadius();
				minIndex = idx;
			}
			idx++;
			
			stop = stop || idx >= node.getNumberOfEntries();	
		}
		
		return minIndex;
	}
}
