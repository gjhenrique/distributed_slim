package br.uel.cross.parallel.mams.subtreechooser;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.IndexEntryTree;
import br.uel.cross.parallel.mams.model.IndexNodeTree;

public class MinDistSubTreeChooser implements SubTreeChooser {

	@Override
	public int subtreeChooser(IndexNodeTree node, float[] element, MetricEvaluator metricEvaluator) {

		int idx = 0, minIndex = 0;

		double minDistance = Double.MAX_VALUE;

		double distance;

		int numberOfEntries = node.getNumberOfEntries();

		boolean stop = (idx >= numberOfEntries);

		int tmpNumberOfEntries = Integer.MAX_VALUE;

		IndexEntryTree entry = null;

		while (!stop) {

			entry = (IndexEntryTree) node.getEntry(idx);

			distance = metricEvaluator.getDistance(element, entry.getSpaceElement());

			if (distance < entry.getConveringRadius()) {
				minDistance = distance;
				stop = true;
				minIndex = idx;
			} else if (distance - entry.getConveringRadius() < minDistance) {
				minDistance = distance - entry.getConveringRadius();
				minIndex = idx;
			}

			idx++;

			stop = stop || (idx >= numberOfEntries);
		}

		while (idx < numberOfEntries) {

			distance = metricEvaluator.getDistance(element, entry.getSpaceElement());

			if (distance < entry.getConveringRadius() && (distance < minDistance)) {
				minDistance = distance;
				minIndex = idx;
			}

			idx++;
		}
		return minIndex;
	}
}
