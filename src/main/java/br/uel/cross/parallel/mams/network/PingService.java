package br.uel.cross.parallel.mams.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PingService implements ParallelService {

	private List<HostSocket> hosts;

	public PingService(List<HostSocket> hosts) {
		this.hosts = hosts;
	}

	@Override
	public void startService() {

		for (final HostSocket host : hosts) {

			ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
			exec.scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
					try {
						boolean active = InetAddress.getByName(host.getHostName()).isReachable(200);
						if (!active) {
							host.setActive(false);
						}
					} catch (UnknownHostException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}, 0, 1, TimeUnit.SECONDS);
		}

	}

	@Override
	public void stopService() {
		// TODO Auto-generated method stub

	}
}
