package br.uel.cross.parallel.mams.split;

import java.util.List;
import java.util.Random;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.util.LogicNode;

public class RandomPromoter implements Promoter{

	@Override
	public void promote(LogicNode logicNode, MetricEvaluator metricEvaluator) {
		
		int idx1, idx2;
	
	    Random rand = new Random();

	    int numberOfEntries = logicNode.getLogicEntries().size();
		
		idx1 = rand.nextInt(numberOfEntries-1);
		
		do{
			
			idx2 = rand.nextInt(numberOfEntries-1);
			
		}while(idx1 == idx2);
		
		logicNode.setRepresentatives(idx1, idx2);
	}

	@Override
	public void distribute(LogicNode logicNode, NodeTree oldNode, float[] lRep, NodeTree newNode,
			float[] rRep, MetricEvaluator metricEvaluator) {
		
		// Delegando para o nó lógico a distribuição dos nós
		logicNode.distribute(oldNode, lRep, newNode, rRep, metricEvaluator);
		
	}
}