package br.uel.cross.parallel.mams.distance;


public interface MetricEvaluator {
	
	public double getDistance(float[] obj1, float[] obj2);
	
}
