package br.uel.cross.parallel.mams.result;

import java.io.Serializable;
import java.util.Comparator;

public class ResultPair implements Comparator<ResultPair>, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 67629311405862738L;

	private long rowId;
	
	private double distance;
	
	public ResultPair(long rowId, double distance){
		this.rowId = rowId;
		this.distance = distance;
	}
	
	public ResultPair(){
		this(-1, -1);
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public int compare(ResultPair o1, ResultPair o2) {
		
		if(o1.getDistance() == o2.getDistance())
			return -1;
		
		return o1.getDistance() < o2.getDistance() ? -1
	            : o1.getDistance() > o2.getDistance() ? 1
	            : 0;
	}
}
