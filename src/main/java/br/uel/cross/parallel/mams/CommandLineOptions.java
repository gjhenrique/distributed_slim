package br.uel.cross.parallel.mams;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

public class CommandLineOptions {


    private Options options;

    public CommandLineOptions(Options options) {
        this.options = options;
    }


    public void addOptionsTreeBuilding() {
        Option pageManagerOption = this.createOptionWithArgs("pm", "page-manager", "Tipo do Page Manager (Disk*, Hdfs, File)", "type", String.class);
        options.addOption(pageManagerOption);

        Option subTreeChooserOption = this.createOptionWithArgs("sub", "subtree-chooser", "Algoritmo de escolha da subárvore (MinOccup*, MinDist, Biased)", "type", String.class);
        options.addOption(subTreeChooserOption);

        Option serializerOption = this.createOptionWithArgs("ser", "serializer", "Tipo de Serializer (Kryo*, Java)", "type", String.class);
        options.addOption(serializerOption);

        Option distanceOption = this.createOptionWithArgs("d", "distance", "Tipo de Função de Distância (Euclidiana*)", "type", String.class);
        options.addOption(distanceOption);

        Option splitOption = this.createOptionWithArgs("s", "split", "Tipo de Split (Mst*, MinMax, Random)", "type", String.class);
        options.addOption(splitOption);

        Option cacheOption = this.createOptionWithArgs("c", "cache", "Usar Cache", "numero de elementos", Number.class);
        options.addOption(cacheOption);

        Option blockSizeOption = this.createOptionWithArgs("b", "block", "Tamanho de Bloco (em Bytes)", "tamanho", Number.class);
        options.addOption(blockSizeOption);
    }

    public void addOptionsOperations() {
        options.addOption("p", "parallel", false, "Operação distribuída");
        options.addOption("del", "delete", false, "Deletar índices da slim");

        Option knnOption = this.createOptionWithArgs("k", "knn", "Operação knn", "k-ésimo", Number.class);
        options.addOption(knnOption);

        Option rangeOption = this.createOptionWithArgs("rq", "range-query", "Operação Range Query", "range", Number.class);
        options.addOption(rangeOption);

        Option createOption = this.createOptionWithArgs("cr", "create", "Criação da Estrutura", "text_file", String.class, 2);
        options.addOption(createOption);


        Option fileCreationOption = this.createOptionWithArgs("fi", "file_creation", "Nome do Índice para construção ou operações", "file_name", String.class);
        options.addOption(fileCreationOption);

        Option fileOperationOption = this.createOptionWithArgs("fo", "file_operation", "Nome ou Caminho para os pontos da operaçao", "file_name", String.class);
        options.addOption(fileOperationOption);
    }

    public Option createOptionWithArgs(String shortName, String longName, String description, String argName, Class clazz) {
        return createOptionWithArgs(shortName, longName, description, argName, clazz, 1);
    }

    public Option createOptionWithArgs(String shortName, String longName, String description, String argName, Class clazz, int argsNumber) {
        return OptionBuilder.
                withArgName(argName).
                withType(clazz).
                withLongOpt(longName).
                withDescription(description).
                hasArgs(argsNumber).
                create(shortName);
    }

    public Options getOptions() {
        return options;
    }

}
