package br.uel.cross.parallel.mams.result;

public enum QueryType {

	
	UNKNOWN,

	RANGEQUERY,

	PARALLELRANGEQUERY,
	
	KNEARESTQUERY
}
