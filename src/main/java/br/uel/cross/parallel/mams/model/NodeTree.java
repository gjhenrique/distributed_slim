package br.uel.cross.parallel.mams.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.uel.cross.parallel.util.EntrySizeMeasurer;

public abstract class NodeTree implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8369478018922415153L;

	protected List<EntryTree> entries = new ArrayList<EntryTree>();

	protected long blockId;

	public NodeTree() {
	}

	public NodeTree(long fileId) {
		this.blockId = fileId;
	}
	
	public boolean addEntry(EntryTree entry) {

		int maxEntries = (entry instanceof LeafEntryTree) ? EntrySizeMeasurer
				.getNumberOfLeafEntries(getEntry()) : EntrySizeMeasurer.getNumberOfIndexEntries(getEntry());
			
		if (getEntry().size() >= maxEntries) {
			return false;
		} else {
			entries.add(entry);
			return true;
		}
	}

	public List<EntryTree> getEntry() {
		return entries;
	}

	public EntryTree getEntry(int index) {
		return entries.get(index);
	}

	public void setEntry(List<EntryTree> entries) {
		this.entries = entries;
	}

	public abstract boolean isLeaf();

	public void setBlockId(long blockId) {
		this.blockId = blockId;
	}

	public long getBlockId() {
		return this.blockId;
	}

	public boolean entryFitNode(EntryTree entry) {
		return false;
	}

	public int getNumberOfEntries() {
		return entries.size();
	}

	public void removeAll() {

		this.entries.clear();

	}

	public String toString() {

		String tipo = (this instanceof LeafNodeTree) ? "NÓ FOLHA " + blockId : "NÓ ÍNDICE " + blockId;

		return tipo + "\nNúmero de Entradas ==> " + entries.size() + "\nEntradas  ==> " + entries;
	}

	public EntryTree getLastEntry() {
		return entries.get(entries.size() - 1);
	}

	public abstract EntryTree createEntry();

	public abstract double getMinimumRadius();

	public abstract int getTotalObjectCount();
}
