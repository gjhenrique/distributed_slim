package br.uel.cross.parallel.mams.model;


public class LeafNodeTree extends NodeTree {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5507142027956494791L;
	
	public LeafNodeTree(){
		super();
	}
	
	public LeafNodeTree(long fileId){
		
		super(fileId);
		
	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public EntryTree createEntry() {
		return new LeafEntryTree();
	}
	

	public double getMinimumRadius() {

		double radius= 0;
		
		for(int i = 0; i < entries.size(); i++){
			if(radius < entries.get(i).getRepresentativeDistance())
				radius = entries.get(i).getRepresentativeDistance();
		}
		
		return radius;
	}

	@Override
	public int getTotalObjectCount() {
		return getNumberOfEntries();
	}
	
}
