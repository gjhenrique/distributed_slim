package br.uel.cross.parallel.mams.statiscs;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class StatsAttribute implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ConcurrentMap<StatsType, AtomicLong> statsProperties = new ConcurrentHashMap<StatsType, AtomicLong>();

	private ConcurrentMap<StatsType, AtomicInteger> statsCounterAverage = new ConcurrentHashMap<StatsType, AtomicInteger>();
	private ConcurrentMap<StatsType, AtomicLong> statsSumAverage = new ConcurrentHashMap<StatsType, AtomicLong>();

	public StatsAttribute() {

		for (StatsType statsType : StatsType.values()) {
			statsProperties.put(statsType, new AtomicLong(0));
			statsCounterAverage.put(statsType, new AtomicInteger(0));
			statsSumAverage.put(statsType, new AtomicLong(0));
		}

	}

	public void incrementStats(StatsType type) {
		this.statsProperties.get(type).incrementAndGet();
	}

	public void averageStats(StatsType type, long value) {

		int numberElements = this.statsCounterAverage.get(type).incrementAndGet();
		long sumValue = this.statsSumAverage.get(type).get();

		long updatedValue = (value + sumValue) / numberElements;

		this.statsProperties.get(type).set(updatedValue);
		this.statsCounterAverage.get(type).set(numberElements);
		this.statsSumAverage.get(type).addAndGet(value);
	}

	public void setStats(StatsType type, long value) {
		this.statsProperties.get(type).set(value);
	}

	public void addStats(StatsType type, long value) {
		this.statsProperties.get(type).addAndGet(value);
	}

	public long getStatsAttribute(StatsType type) {
		return this.statsProperties.get(type).longValue();
	}
	
	@Override
	public String toString(){
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("\n");
		for (StatsType type : StatsType.values()) {
			int value = statsProperties.get(type).intValue();

			if (value != 0)
				builder.append("\n" + type.writeStats(value));
		}
		
		return builder.toString();
	}
	
}
