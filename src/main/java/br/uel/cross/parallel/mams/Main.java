package br.uel.cross.parallel.mams;

import br.uel.cross.parallel.util.EntrySizeMeasurer;
import org.apache.commons.cli.*;

public class Main {

    public Main(String[] args) {

        Options options = createOptions();
        CommandLine cmd = createCommandLineProcessor(options, args);
        executeProgram(cmd, options);
    }

    public static void main(String[] args) throws Exception {
        new Main(args);
    }

    private Options createOptions() {
        Options options = new Options();

        options.addOption("h", "help", false, "Exibe essa ajuda");

        CommandLineOptions cmdOptions = new CommandLineOptions(options);
        cmdOptions.addOptionsTreeBuilding();
        cmdOptions.addOptionsOperations();

        return options;
    }

    private CommandLine createCommandLineProcessor(Options options, String[] args) {

        Parser parser = new PosixParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return cmd;
    }

    private void executeProgram(CommandLine cmd, Options options) {
        CommandLineHandler cmdHandler = new CommandLineHandler(cmd);

        if (cmd.hasOption('h') || cmd.getOptions().length == 0) {
            HelpFormatter f = new HelpFormatter();
            f.printHelp("Distributed Slim", options);
        } else {
            //      Atributos estáticos sujeiras
            String serializerOptionValue = (cmd.hasOption("ser")) ? cmd.getOptionValue("ser") : "Kryo";
            EntrySizeMeasurer.serializer = MamFactory.createSerializer(serializerOptionValue);

            int defaultBlockSize = (cmd.hasOption("b")) ? Integer.parseInt(cmd.getOptionValue("b")) : 4096;
            EntrySizeMeasurer.defaultBlockSize = defaultBlockSize;

            MamTree tree = cmdHandler.buildTree();
            cmdHandler.executeOperations(tree);
        }
    }
}
