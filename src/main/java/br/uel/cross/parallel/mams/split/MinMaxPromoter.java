package br.uel.cross.parallel.mams.split;

import java.util.List;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.IndexNodeTree;
import br.uel.cross.parallel.mams.model.LeafNodeTree;
import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.util.LogicNode;
import br.uel.cross.parallel.util.LogicNode.NodeType;

public class MinMaxPromoter implements Promoter{

	@Override
	public void promote(LogicNode logicNode, MetricEvaluator metricEvaluator) {
		
		double iRadius, jRadius, min;
		
		int idx1 = 0, idx2 = 0;
		
		min = Double.MAX_VALUE;

		int numberOfEntries = logicNode.getCount();
		
		NodeTree node1 = null;
		NodeTree node2 = null;
		
		if(logicNode.getNodeType() == NodeType.INDEXNODE){
			node1 = new IndexNodeTree();
			node2 = new IndexNodeTree();
		}
		else{

			node1 = new LeafNodeTree();
			node2 = new LeafNodeTree();
		}
		
		for(int i = 0; i < numberOfEntries; i++){
			for(int j = i + 1; j < numberOfEntries; j++){
				logicNode.setRepresentatives(i, j);
				node1.removeAll();
				node2.removeAll();
				
				logicNode.testDistribution(node1, node2, metricEvaluator);
				
				iRadius = node1.getMinimumRadius();
				jRadius = node2.getMinimumRadius();

				if(iRadius < jRadius){
					iRadius = jRadius; 
				}

				if(iRadius < min){
					min = iRadius;
					idx1 = i;
					idx2 = j;
				}
			}
		} 
		logicNode.setRepresentatives(idx1, idx2);
	}

	@Override
	public void distribute(LogicNode logicNode, NodeTree oldNode, float[] lRep, NodeTree newNode,
			float[] rRep, MetricEvaluator metricEvaluator) {
		
		//Delegando para a distribuição do logic node
		logicNode.distribute(oldNode, lRep, newNode, rRep, metricEvaluator);
		
	}
}
