package br.uel.cross.parallel.mams.pagemanager;

import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.model.IndexEntryTree;
import br.uel.cross.parallel.mams.model.LeafEntryTree;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class EntryTreeSerializer extends com.esotericsoftware.kryo.Serializer<EntryTree> {

	@Override
	public void write(Kryo kryo, Output output, EntryTree object) {

		boolean isLeaf = (object instanceof LeafEntryTree) ? true : false;

		output.writeBoolean(isLeaf);

		output.writeDouble(object.getRepresentativeDistance());

		float[] elements = object.getSpaceElement();

		output.writeInt(elements.length);

		for (float element : elements) {
			output.writeFloat(element);
		}

		this.writeChildProperties(output, object);

	}

	private void writeChildProperties(Output output, EntryTree object) {

		if (object instanceof LeafEntryTree) {

			LeafEntryTree leaf = (LeafEntryTree) object;

			output.writeLong(leaf.getRowId());

		} else if (object instanceof IndexEntryTree) {

			IndexEntryTree indexEntryTree = (IndexEntryTree) object;

			output.writeDouble(indexEntryTree.getConveringRadius());
			output.writeLong(indexEntryTree.getPtr());
			output.writeInt(indexEntryTree.getnEntries());

		} else {

			throw new RuntimeException("Não pertence ao universo do EntryTree");

		}
	}

	@Override
	public EntryTree read(Kryo kryo, Input input, Class<EntryTree> type) {
		
		boolean isLeaf = input.readBoolean();
		
		double representativeDistance = input.readDouble();

		int elementsSize = input.readInt();

		float[] elements = new float[elementsSize];

		for(int i = 0; i < elementsSize; i++){
			
			float element = input.readFloat();
			elements[i] = element;
		}
		
		EntryTree entry = this.readChildProperties(isLeaf, input);
		
		entry.setRepresentativeDistance(representativeDistance);
		entry.setSpaceElement(elements);

		return entry;
	}

	private EntryTree readChildProperties(boolean isLeaf, Input input) {

		EntryTree entry = null;
		
		if(isLeaf){
			
			entry = new LeafEntryTree();
			
			LeafEntryTree leaf = (LeafEntryTree) entry;
			leaf.setRowId(input.readLong());
		}
		else {
			
			entry = new IndexEntryTree();
			IndexEntryTree indexEntry = (IndexEntryTree) entry;
			
			indexEntry.setConveringRadius(input.readDouble());
			indexEntry.setPtr(input.readLong());
			indexEntry.setnEntries(input.readInt());

		}
		
		return entry;
	}

}
