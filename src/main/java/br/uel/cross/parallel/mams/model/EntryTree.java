package br.uel.cross.parallel.mams.model;

import java.io.Serializable;

public abstract class EntryTree implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1938924243361100755L;

	private float[] elements;
	
	private double representativeDistance;

	public EntryTree(){
		
	}
	
	public EntryTree(float[] elements, double representativeDistance){
		this.elements = elements;
		this.representativeDistance = representativeDistance;
	}
	
	public double getRepresentativeDistance() {
		return representativeDistance;
	}

	public void setRepresentativeDistance(double representativeDistance) {
		this.representativeDistance = representativeDistance;
	}

	public float[] getSpaceElement() {
		return elements;
	}

	public void setSpaceElement(float[] elements) {
		this.elements = elements;
	}

	public String toString(){
		String childProperties;
		
		if(this instanceof LeafEntryTree){
			LeafEntryTree leafEntry = (LeafEntryTree) this;
			childProperties = "\nRowId ==> " + leafEntry.getRowId(); 
			
		}else{
			IndexEntryTree internalEntry = (IndexEntryTree) this;
			childProperties = "\nPonteiro ==> " + internalEntry.getPtr()+
							"\nRaio ==> " + internalEntry.getConveringRadius()+
							"\nNúmero de Entradas ==> " + internalEntry.getnEntries();
		}
		
		return "Elementos ==> " + elements +
				"\nDistância do Representativo ==> " + representativeDistance+
				childProperties;
	}
	
}
