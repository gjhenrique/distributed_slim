package br.uel.cross.parallel.mams;

import br.uel.cross.parallel.mams.distance.EuclideanMetricEvaluator;
import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.pagemanager.*;
import br.uel.cross.parallel.mams.split.MinMaxPromoter;
import br.uel.cross.parallel.mams.split.MstSplitterPromoter;
import br.uel.cross.parallel.mams.split.Promoter;
import br.uel.cross.parallel.mams.split.RandomPromoter;
import br.uel.cross.parallel.mams.subtreechooser.BiasedSubTreeChooser;
import br.uel.cross.parallel.mams.subtreechooser.MinDistSubTreeChooser;
import br.uel.cross.parallel.mams.subtreechooser.MinOccupancySubTreeChooser;
import br.uel.cross.parallel.mams.subtreechooser.SubTreeChooser;
import com.esotericsoftware.kryo.Kryo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

public class MamFactory {

    private static String HADOOP_FILE = "/usr/local/hadoop/conf/core-site.xml";
    private static String HDFS_FILE = "/usr/local/hadoop/conf/hdfs-site.xml";

    public static PageManager createPageManager(String pageManagerId, Serializer serializer, String path) {
        PageManager pageManager = null;

        switch (pageManagerId) {
            case "Hdfs":
                pageManager = new HdfsPageManager(loadHdfs(path), serializer);
                break;
            case "File":
                pageManager = new FilePageManager(path, serializer);
                break;
            case "Disk":
                pageManager = new DiskPageManager(path, serializer);
                break;
            default:
                throw new IllegalArgumentException("Page Manager " + pageManagerId + " desconhecido");
        }
        return pageManager;
    }

    /*
     Método que leva em consideração o tamanho do bloco
     Só o DiskPageManager leva em consideração esse tamanho para construir o novo índice
     */
    public static PageManager createPageManager(String pageManagerId, Serializer serializer, String path, int defaultBlockSize) {
        PageManager pageManager = null;

        switch (pageManagerId) {
            case "Disk":
                pageManager = new DiskPageManager(path, serializer, defaultBlockSize);
                break;
            case "File":
                pageManager = createPageManager(pageManagerId, serializer, path);
                break;
            case "Hdfs":
                pageManager = createPageManager(pageManagerId, serializer, path);
                break;
            default:
                throw new IllegalArgumentException("Page Manager " + pageManagerId + " desconhecido");
        }

        return pageManager;
    }

    public static MetricEvaluator createMetricEvaluator(String evaluatorId) {
        MetricEvaluator evaluator = null;
        switch (evaluatorId) {
            case "Euclidiana":
                evaluator = new EuclideanMetricEvaluator();
                break;
            default:
                throw new IllegalArgumentException("Função de distância " + evaluatorId + " desconhecida");
        }
        return evaluator;
    }

    public static Promoter createPromoter(String promoterId) {
        Promoter promoter = null;

        switch (promoterId) {
            case "Mst":
                promoter = new MstSplitterPromoter();
                break;
            case "MinMax":
                promoter = new MinMaxPromoter();
                break;
            case "Random":
                promoter = new RandomPromoter();
                break;
            default:
                throw new IllegalArgumentException("Tipo de split " + promoterId + " desconhecido");

        }
        return promoter;
    }

    public static SubTreeChooser createSubtreeChooser(String chooserId) {
        SubTreeChooser chooser = null;
        switch (chooserId) {
            case "MinOccup":
                chooser = new MinOccupancySubTreeChooser();
                break;
            case "MinDist":
                chooser = new MinDistSubTreeChooser();
                break;
            case "Biased":
                chooser = new BiasedSubTreeChooser();
                break;
            default:
                throw new IllegalArgumentException("Tipo de escolha de subarvore " + chooserId + " não reconhecida");
        }
        return chooser;
    }

    public static Serializer createSerializer(String serializerId) {
        Serializer serializer = null;
        switch (serializerId) {
            case "Kryo":
                serializer = new KryoSerializer<>(new Kryo(), ArrayList.class);
                break;
            case "Java":
                serializer = new JavaSerializer();
                break;
        }
        return serializer;
    }


    private static FileSystem loadHdfs(String uri) {

        Configuration conf = new Configuration();
        conf.addResource(new Path("/usr/local/hadoop/conf/core-site.xml"));
        conf.addResource(new Path("/usr/local/hadoop/conf/hdfs-site.xml"));

        FileSystem fs = null;
        try {
            fs = FileSystem.get(URI.create(uri), conf);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fs;
    }

}
