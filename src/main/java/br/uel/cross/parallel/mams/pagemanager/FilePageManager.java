package br.uel.cross.parallel.mams.pagemanager;

import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.util.NodeSimpleFactory;

import java.io.*;
import java.util.List;

public class FilePageManager implements PageManager {

    private final static String ROOT_FILE_NAME = "ROOT";
    private long usedPages;
    private String relativePath;
    private File rootFile;
    private Serializer serializer;

    public FilePageManager(String relativePath, Serializer serializer) {
        this.relativePath = relativePath;

        this.createHeader();

        this.serializer = serializer;
    }

    @Override
    public NodeTree findRoot() {

        if (!this.rootFile.isFile()) {
            return null;
        }

        long blockId = this.getRootBlockId();

        return this.fromFileToNode(blockId);
    }

    private long getRootBlockId() {

        long blockId = 0;

        try {
            FileInputStream fis = new FileInputStream(rootFile);
            DataInputStream dis = new DataInputStream(fis);

            blockId = dis.readLong();

            fis.close();
            dis.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return blockId;
    }

    @Override
    public long getBlockId() {
        return usedPages++;
    }

    @Override
    public void writeNode(long blockId, NodeTree nodeTree) {

        try {

            FileOutputStream fos = new FileOutputStream(relativePath + File.separator + blockId);
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            byte[] headerFile = new byte[]{(byte) (nodeTree.isLeaf() ? 1 : 0)};
            bos.write(headerFile);

            this.serializer.write(bos, nodeTree.getEntry());

            bos.flush();
            fos.close();
            bos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void changeRootNode(long blockId, int height) {

        FileOutputStream fos = null;
        DataOutputStream dos = null;
        try {
            fos = new FileOutputStream(rootFile);
            dos = new DataOutputStream(fos);

            dos.writeLong(blockId);
            dos.writeInt(height);

            fos.close();
            dos.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public synchronized NodeTree fromFileToNode(long blockId) {

        NodeTree nodeTree = null;

        FileInputStream fis;
        try {

            fis = new FileInputStream(relativePath + File.separator + blockId);

            DataInputStream dis = new DataInputStream(fis);
            BufferedInputStream bis = new BufferedInputStream(dis);

            boolean isLeaf = (dis.readByte() != 0);

            List<EntryTree> entryList = (List<EntryTree>) this.serializer.read(bis);

            nodeTree = NodeSimpleFactory.create(isLeaf);
            nodeTree.setBlockId(blockId);
            nodeTree.setEntry(entryList);

            fis.close();
            dis.close();
            bis.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return nodeTree;
    }

    @Override
    public int getTreeHeight() {

        int height = 0;

        try {

            FileInputStream fis = new FileInputStream(rootFile);
            DataInputStream dis = new DataInputStream(fis);

            // Lendo desnessário id do bloco
            dis.readLong();
            height = dis.readInt();

            dis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return height;
    }

    @Override
    public boolean pageExists(long blockId) {
        File file = new File(relativePath + File.separator + blockId);
        return file.exists();
    }

    @Override
    public void deleteIndex() {
        File folderIndex = new File(relativePath);
        File[] files = folderIndex.listFiles();
        if (files != null) {
            for (File file : files) {
                file.delete();
            }
        }

        this.createHeader();
    }

    private void createHeader() {
        this.rootFile = new File(relativePath + File.separator + ROOT_FILE_NAME);
        this.usedPages = 0;
    }
}
