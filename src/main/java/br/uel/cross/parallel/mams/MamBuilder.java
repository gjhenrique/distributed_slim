package br.uel.cross.parallel.mams;

import br.uel.cross.parallel.mams.distance.EuclideanMetricEvaluator;
import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.pagemanager.MemoryPageManager;
import br.uel.cross.parallel.mams.pagemanager.PageManager;
import br.uel.cross.parallel.mams.split.MstSplitterPromoter;
import br.uel.cross.parallel.mams.split.Promoter;
import br.uel.cross.parallel.mams.subtreechooser.MinDistSubTreeChooser;
import br.uel.cross.parallel.mams.subtreechooser.SubTreeChooser;

public class MamBuilder {

    private PageManager pageManager;

    private MetricEvaluator metricEvaluator;

    private Promoter promoter;

    private SubTreeChooser subTreeChooser;

    public MamBuilder() {

//        Valores default
        this.metricEvaluator = new EuclideanMetricEvaluator();
        this.subTreeChooser = new MinDistSubTreeChooser();
        this.promoter = new MstSplitterPromoter();
        this.pageManager = new MemoryPageManager();

    }

    public MamBuilder withPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
        return this;
    }

    public MamBuilder withMetricEvaluator(MetricEvaluator metricEvaluator) {
        this.metricEvaluator = metricEvaluator;
        return this;
    }

    public MamBuilder withPromoter(Promoter promoter) {
        this.promoter = promoter;
        return this;
    }

    public MamBuilder withSubTreeChooser(SubTreeChooser chooser) {
        this.subTreeChooser = chooser;
        return this;
    }

    public MamTree create() {
        return new MamTree(pageManager, metricEvaluator, promoter, subTreeChooser);
    }

}
