package br.uel.cross.parallel.mams.statiscs;

import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.mams.operation.MamOperationParallelRangeQuery;
import br.uel.cross.parallel.mams.pagemanager.GuavaCachePageManagerDecorator;
import br.uel.cross.parallel.mams.result.TreeResult;
import br.uel.cross.parallel.mams.server.ServerRangeQueryHandler;
import com.google.common.cache.CacheStats;
import com.google.common.primitives.Ints;

import java.io.PrintStream;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public aspect StatiscsAspect {

	private QueryStats stats;

	private int queryNumber;

	pointcut initializeQueryStats(String file) : call(public void br.uel.cross.parallel.mams.operation.MamOperation.performAction(..)) && args(file);

	pointcut sumDistanceComputations() : execution(double br.uel.cross.parallel.mams.distance.EuclideanMetricEvaluator.getDistance(..));

	pointcut getDiskAccessReading() : execution(* br.uel.cross.parallel.mams.pagemanager.FilePageManager.fromFileToNode(..))
		|| execution(* br.uel.cross.parallel.mams.pagemanager.DiskPageManager.fromFileToNode(..))
		|| execution(* br.uel.cross.parallel.mams.pagemanager.HdfsPageManager.fromFileToNode(..))
        || execution(* br.uel.cross.parallel.mams.pagemanager.MemoryPageManager.fromFileToNode(..));

	pointcut getDiskAccessWriting() :execution(* br.uel.cross.parallel.mams.pagemanager.FilePageManager.writeNode(..))
	        || execution(* br.uel.cross.parallel.mams.pagemanager.DiskPageManager.writeNode(..))
            || execution(* br.uel.cross.parallel.mams.pagemanager.HdfsPageManager.writeNode(..))
            || execution(* br.uel.cross.parallel.mams.pagemanager.MemoryPageManager.writeNode(..));

	pointcut performLocalAction() : call(TreeResult br.uel.cross.parallel.mams.operation.MamOperationLocal.performActionLocal(..));

	pointcut processAllElements() : call(private void br.uel.cross.parallel.mams.operation.MamOperation.processElements(..));

	pointcut processCacheStats() : execution(void br.uel.cross.parallel.mams.pagemanager.GuavaCachePageManagerDecorator.fromFileToNode(..)) ||
	execution(void GuavaCachePageManagerDecorator.writeNode(..)) ;

	pointcut registerNewQuery() : execution(void  br.uel.cross.parallel.mams.operation.MamOperationParallelRangeQuery.performActionMam(..));

	pointcut handleParallelQueryResult(TreeResult result, String identifier, List<QueryStats> listStats) :
		execution(void br.uel.cross.parallel.mams.operation.MamOperationParallelRangeQuery.handleQueryResult(..))
		&& args(result, identifier, listStats);

	pointcut setQuery() : execution(QueryStats br.uel.cross.parallel.mams.server.ServerRangeQueryHandler.getQueryStats(..));

	pointcut initializeStatsServer() : execution(void br.uel.cross.parallel.mams.server.ServerRangeQueryHandler.handle(..));

	pointcut getObjectAccessTime() : execution( * br.uel.cross.parallel.mams.pagemanager.Serializer.read(..));

	private ConcurrentMap<String, Long> queryInCourse = new ConcurrentHashMap<String, Long>();

	before() : sumDistanceComputations() {
		if (this.stats != null)
			this.stats.incrementStats(StatsType.DISTANCE_COMPUTATION);
	}

	before(String file) : initializeQueryStats(file){
		this.stats = new QueryStats();
		this.stats.setFileName(file);
	}

	after() : processCacheStats(){
		GuavaCachePageManagerDecorator finder = (GuavaCachePageManagerDecorator) thisJoinPoint.getThis();

		CacheStats cacheStats = finder.getStats();

		long cacheHit = cacheStats.hitCount();
		long cacheMiss = cacheStats.missCount();

		stats.setStats(StatsType.CACHE_HIT, Ints.checkedCast(cacheHit));
		stats.setStats(StatsType.CACHE_MISS, Ints.checkedCast(cacheMiss));
	}

	NodeTree around(): getDiskAccessReading(){
		long start = System.nanoTime();

		NodeTree node = proceed();

		long end = System.nanoTime() - start;

		if (this.stats != null) {
			this.stats.incrementStats(StatsType.DISK_ACCESS_READING);
			this.stats.averageStats(StatsType.AVG_DISK_ELEMENTS_READING, node.getNumberOfEntries());
			this.stats.averageStats(StatsType.AVG_TIME_ACCESS_DISK_READING, end);
		}

		return node;
	}

	void around() : getDiskAccessWriting(){
		NodeTree node = (NodeTree) thisJoinPoint.getArgs()[1];
		long start = System.nanoTime();

		proceed();

		long end = System.nanoTime() - start;

		if (this.stats != null) {
			this.stats.incrementStats(StatsType.DISK_ACCESS_WRITING);
			this.stats.averageStats(StatsType.AVG_DISK_ELEMENTS_WRITING, node.getNumberOfEntries());
			this.stats.averageStats(StatsType.AVG_TIME_ACCESS_DISK_WRITING, end);
		}
	}

	void around() : processAllElements(){

		this.stats.startGlobalTime();

		proceed();

		this.stats.finishGlobalTime();

		// this.stats.setStats(StatsType.MAX_ENTRIES_INDEX_NODE,
		// EntrySizeMeasurer.getNumberOfIndexEntries());
		// this.stats.setStats(StatsType.MAX_ENTRIES_LEAF_NODE,
		// EntrySizeMeasurer.getNumberOfLeafEntries());

		this.writeStats();
	}

	TreeResult around() : performLocalAction(){
		long start = System.nanoTime();

		TreeResult result = proceed();
		long end = System.nanoTime() - start;

		this.queryNumber++;

		this.stats.addResut(queryNumber + "", end, result);

		return result;
	}

	before() : registerNewQuery(){
		MamOperationParallelRangeQuery operation = (MamOperationParallelRangeQuery) thisJoinPoint.getThis();

		long start = System.nanoTime();
		this.queryInCourse.put(operation.getIdentifier(), start);
	}

	void around(TreeResult result, String identifier, List<QueryStats> listStats) : handleParallelQueryResult(result, identifier, listStats){
		long start = this.queryInCourse.get(identifier);
		long end = System.nanoTime();

		this.stats.addResut(identifier, end - start, result);

		for (QueryStats statsIt : listStats) {
			this.stats.mergeStats(statsIt);
		}

		this.queryInCourse.remove(identifier);

		if (this.queryInCourse.size() == 0) {
			this.stats.finishGlobalTime();
			this.writeStats(listStats);
		}
		proceed(result, identifier, listStats);
	}

	before() : setQuery(){
		ServerRangeQueryHandler handler = (ServerRangeQueryHandler) thisJoinPoint.getThis();
		handler.stats = this.stats;
	}

	before() : initializeStatsServer(){
		this.stats = new QueryStats();

		try {
			String localhostname = java.net.InetAddress.getLocalHost().getHostName();
			this.stats.setHostName(localhostname);
		} catch (UnknownHostException uhe) {
			uhe.printStackTrace();
		}

	}

    private void writeStats() {
        PrintStream printStream = this.getPrintStream();
        printStream.print(stats);
    }

    private void writeStats(List<QueryStats> listStats) {
        PrintStream printStream = this.getPrintStream();
        printStream.print(stats);
        printStream.print(stats.getStringHostAttributes());
        printStream.close();
    }

    private PrintStream getPrintStream() {
        return System.out;
    }

}

