package br.uel.cross.parallel.mams.network.operations;

import br.uel.cross.parallel.mams.network.HostSocket;
import br.uel.cross.parallel.mams.server.ProtocolCode;

public class InitialRangeQueryOperation extends ParallelOperation{

	float[] sample;
	
	double range;
	
	public InitialRangeQueryOperation(HostSocket host, ParallelOperationCallback operation, float[] sample, double range){
		super(host, operation, 0);
		this.sample = sample;
		this.range = range;
	}
	
	@Override
	public Object performOperation() {
		host.initializeRangeQuery(sample, range);
		return null;
	}

	@Override
	public ProtocolCode getServerOperation(){
		return ProtocolCode.INITIALRANGEQUERY;
	}
	
	
	
}
