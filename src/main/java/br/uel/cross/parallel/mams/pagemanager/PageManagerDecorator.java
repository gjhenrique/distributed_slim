package br.uel.cross.parallel.mams.pagemanager;

import br.uel.cross.parallel.mams.model.NodeTree;

public abstract class PageManagerDecorator implements PageManager{

	protected PageManager pageManager;
	
	public PageManagerDecorator(PageManager pageManager){
		this.pageManager = pageManager;
	}
	
	@Override
	public NodeTree findRoot() {
		return pageManager.findRoot();
	}

	@Override
	public long getBlockId() {
		return pageManager.getBlockId();
	}

	@Override
	public void writeNode(long blockId, NodeTree nodeTree) {
		pageManager.writeNode(blockId, nodeTree);
	}

	@Override
	public void changeRootNode(long blockId, int height) {
		pageManager.changeRootNode(blockId, height);
	}

	@Override
	public NodeTree fromFileToNode(long blockId) {
		return pageManager.fromFileToNode(blockId);
	}

	@Override
	public int getTreeHeight() {
		return pageManager.getTreeHeight();
	}
	
	@Override
	public boolean pageExists(long blockId) {
		return pageManager.pageExists(blockId);
	}

    @Override
    public void deleteIndex() {
        pageManager.deleteIndex();
    }
}
