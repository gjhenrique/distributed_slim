package br.uel.cross.parallel.mams.subtreechooser;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.IndexEntryTree;
import br.uel.cross.parallel.mams.model.IndexNodeTree;

public class MinOccupancySubTreeChooser implements SubTreeChooser{

	@Override
	public int subtreeChooser(IndexNodeTree node, float[] element,
			MetricEvaluator metricEvaluator) {
		
		int idx = 0, minIndex = 0;
		
		double minDistance = Double.MAX_VALUE;
		
		double distance;
		
		int numberOfEntries = node.getNumberOfEntries();
		
		boolean stop = (idx >= numberOfEntries);
		
		int tmpNumberOfEntries = Integer.MAX_VALUE;
				
		while(!stop){
			
			IndexEntryTree entryTree = (IndexEntryTree) node.getEntry().get(idx);
			
			distance = metricEvaluator.getDistance(entryTree.getSpaceElement(), element);
			
			if(distance < entryTree.getConveringRadius()){
				
				stop = true;
				minDistance = distance;
				minIndex = idx;
				tmpNumberOfEntries = entryTree.getnEntries();
			}
			else if(distance - entryTree.getConveringRadius() < minDistance){
		        minDistance = distance - entryTree.getConveringRadius();
		        minIndex = idx;
			}
			
			idx++;
			
			stop = stop || (idx >= node.getNumberOfEntries());	
		}
		
		while(idx < numberOfEntries){
			
			IndexEntryTree entryTree = (IndexEntryTree) node.getEntry().get(idx);
			
			distance = metricEvaluator.getDistance(element, entryTree.getSpaceElement());
			
			if( (distance < entryTree.getConveringRadius()) &&
				entryTree.getnEntries() < tmpNumberOfEntries ){
				tmpNumberOfEntries = entryTree.getnEntries();
				minIndex = idx;
			}
			idx++;
		}
		
		return minIndex;
	}

}
