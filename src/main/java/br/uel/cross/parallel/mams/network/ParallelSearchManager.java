package br.uel.cross.parallel.mams.network;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.uel.cross.parallel.util.NetworkConfUtil;
import br.uel.cross.parallel.util.PropertiesUtil;

public class ParallelSearchManager {

	private final static String NETWORK_FILE = "conf/network.properties";
	private final static String HOSTS_FILE = "/usr/local/hadoop/conf/slaves";
	private final static String PORT_SERVER = "PORT-SERVER";
	
	private int portNumber;

	private List<String> hostNames;

	private Map<Long, String[]> locationBlocks;

	public ParallelSearchManager(Map<Long, String[]> locationBlocks) {
		this.locationBlocks = locationBlocks;

		this.portNumber = PropertiesUtil.getIntFromConfiguration(NETWORK_FILE, PORT_SERVER);

		this.hostNames = NetworkConfUtil.getHostsFromHadoop(HOSTS_FILE);
	}

	public ParallelRangeQuery startNewRangeQuery(float[] sample, double range, String identifier,
			QueryResultCallback resultCallback) {

		List<HostSocket> hosts = this.getListOfHosts(identifier);

		ParallelRangeQuery parallelRangeQuery = new ParallelRangeQuery(hosts, locationBlocks, identifier,
				resultCallback);

		parallelRangeQuery.initializeRangeQueryConnections(sample, range);

		return parallelRangeQuery;
	}

	private List<HostSocket> getListOfHosts(String identifier) {

		List<HostSocket> hosts = new ArrayList<HostSocket>();

		for (String hostName : hostNames) {
			HostSocket host = new HostSocket(hostName, portNumber, identifier);
			hosts.add(host);
		}

		return hosts;
	}

}
