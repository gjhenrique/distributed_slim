package br.uel.cross.parallel.mams;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.operation.MamOperation;
import br.uel.cross.parallel.mams.operation.MamOperationInsert;
import br.uel.cross.parallel.mams.operation.MamOperationLocalKnn;
import br.uel.cross.parallel.mams.operation.MamOperationLocalRangeQuery;
import br.uel.cross.parallel.mams.pagemanager.GuavaCachePageManagerDecorator;
import br.uel.cross.parallel.mams.pagemanager.PageManager;
import br.uel.cross.parallel.mams.pagemanager.Serializer;
import br.uel.cross.parallel.mams.split.Promoter;
import br.uel.cross.parallel.mams.subtreechooser.SubTreeChooser;
import org.apache.commons.cli.CommandLine;

import java.io.PrintStream;

public class CommandLineHandler {

    private CommandLine cmd;

    private PrintStream writer;

    public CommandLineHandler(CommandLine cmd) {
        this(cmd, System.out);
    }

    public CommandLineHandler(CommandLine cmd, PrintStream writer) {
        this.cmd = cmd;
        this.writer = writer;
    }

    public MamTree buildTree() {
        MamBuilder builder = new MamBuilder();

        checkPageManager(builder);
        checkSubTree(builder);
        checkMetricEvaluator(builder);
        checkPromoter(builder);

        return builder.create();
    }

    private void checkPageManager(MamBuilder builder) {
        if (cmd.hasOption("pm")) {

            String serializerOptionValue = (cmd.hasOption("ser")) ? cmd.getOptionValue("ser") : "Kryo";
            Serializer serializer = MamFactory.createSerializer(serializerOptionValue);

            String pageManagerId = cmd.getOptionValue("pm");
            String path = cmd.getOptionValue("fi");

            PageManager pageManager = null;
            if (cmd.hasOption("b")) {
                int blockSize = Integer.parseInt(cmd.getOptionValue("b"));
                pageManager = MamFactory.createPageManager(pageManagerId, serializer, path, blockSize);
            } else {
                pageManager = MamFactory.createPageManager(pageManagerId, serializer, path);
            }

//          Uso de cache
            if (cmd.hasOption("c")) {
                int numElements = Integer.parseInt(cmd.getOptionValue("c"));
                pageManager = new GuavaCachePageManagerDecorator(pageManager, numElements);
            }

            builder.withPageManager(pageManager);
        }
    }


    private void checkPromoter(MamBuilder builder) {
        if (cmd.hasOption("s")) {
            String splitOption = cmd.getOptionValue("s");
            Promoter promoter = MamFactory.createPromoter(splitOption);
            builder.withPromoter(promoter);
        }
    }

    private void checkMetricEvaluator(MamBuilder builder) {
        if (cmd.hasOption("d")) {
            String distanceOption = cmd.getOptionValue("d");
            MetricEvaluator evaluator = MamFactory.createMetricEvaluator(distanceOption);
            builder.withMetricEvaluator(evaluator);
        }
    }

    private void checkSubTree(MamBuilder builder) {
        if (cmd.hasOption("sub")) {
            String subTreeOption = cmd.getOptionValue("sub");
            SubTreeChooser chooser = MamFactory.createSubtreeChooser(subTreeOption);
            builder.withSubTreeChooser(chooser);
        }
    }

    public void executeOperations(MamTree tree) {

        String fileOperation = cmd.getOptionValue("fo");
        String fileCreation = cmd.getOptionValue("cr");

        checkForIndexDeletion(tree);
        checkForIndexCreation(fileCreation, tree);
        checkForKnn(fileOperation, tree);
        checkForRangeQuery(fileOperation, tree);
    }

<<<<<<< Updated upstream
    private void checkForIndexDeletion(MAMTree tree) {
        if(cmd.hasOption("del")) {
            tree.getPageManager().deleteIndex();
=======
    private void checkForIndexDeletion(MamTree tree) {
        if (cmd.hasOption("del")) {
            tree.deleteIndex();
>>>>>>> Stashed changes
        }
    }

    private void checkForIndexCreation(String file, MamTree tree) {

        if (cmd.hasOption("cr")) {
            if (file == null) {
                throw new RuntimeException("Especifique o arquivo para criação do índice");
            }

            MamOperation operation = new MamOperationInsert(tree);
            operation.performAction(file);

            if (tree.getPageManager() instanceof GuavaCachePageManagerDecorator) {
                GuavaCachePageManagerDecorator cachePageManager = (GuavaCachePageManagerDecorator) tree.getPageManager();
                cachePageManager.flushCache();
            }

//            TODO Verificar estatisticas do primeiro main e colocar no operation
        }
    }

    private void checkForKnn(String file, MamTree tree) {
//        A knn ainda não tem a sua versão paralela
        if (cmd.hasOption("k")) {

            if(file == null) {
                throw new RuntimeException("Arquivo de consulta da knn não pode ficar vazio");
            }
            int k = Integer.parseInt(cmd.getOptionValue("k"));
            MamOperation operation = new MamOperationLocalKnn(tree, k);
            operation.performAction(file);
        }
    }

<<<<<<< Updated upstream
    private void checkForRangeQuery(String fileOperation, MAMTree tree) {
        if(cmd.hasOption("rq")) {
=======
    private void checkForRangeQuery(String fileOperation, MamTree tree) {
        if (cmd.hasOption("rq")) {
>>>>>>> Stashed changes

            if(cmd.hasOption("p")) {
//                TODO Realizar o modelo paralelo.
//                TODO De preferência, refazer o modelo já implementado com sockets puros
            }
            else {
                double range = Double.parseDouble(cmd.getOptionValue("rq"));
                MamOperation operation = new MamOperationLocalRangeQuery(tree, range);
                operation.performAction(fileOperation);
            }
        }
    }
}
