package br.uel.cross.parallel.mams.server;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.pagemanager.PageManager;

public abstract class ServerHandlerFactory {


	public static ServerHandler create(ProtocolCode protocolCode, PageManager pageManager, MetricEvaluator metricEvaluator){
		ServerHandler handler = null;
		switch(protocolCode){
			case INITIALRANGEQUERY:
				handler = new ServerRangeQueryHandler(pageManager, metricEvaluator);
			break;
			case KNEARESTQUERY:
				
			break;
		}
		return handler;
	}
}
