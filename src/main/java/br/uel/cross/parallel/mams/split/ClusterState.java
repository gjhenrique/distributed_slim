package br.uel.cross.parallel.mams.split;

public enum ClusterState {
	ALIVE,
	DEAD,
	DEATH_SENTENCE
}
