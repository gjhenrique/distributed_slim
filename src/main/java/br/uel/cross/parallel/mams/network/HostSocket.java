package br.uel.cross.parallel.mams.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

import br.uel.cross.parallel.mams.server.ProtocolCode;
import br.uel.cross.parallel.mams.statiscs.QueryStats;

public class HostSocket {

	private int port;

	private Socket socket;

	private ObjectInputStream ois;

	private ObjectOutputStream oos;

	private boolean inicializedServer;

	private String hostName;

	/** Esse host está ocupado processando a requisição */
	private boolean busy;

	private final Object busyLock = new Object();

	/** Esse host está ativo na rede **/
	private boolean active;

	private String queryId;
	
	private final Object activeLock = new Object();

	public HostSocket(String hostName, int portNumber) {
		this.hostName = hostName;

		this.port = portNumber;
	}

	public HostSocket(String hostName, int port, String queryId){
		this.hostName = hostName;
		this.port = port;
		this.queryId = queryId;
	}
	public void connectServer() {

		try {
			socket = new Socket(this.getHostName(), this.port);
			setActive(true);
			oos = new ObjectOutputStream(socket.getOutputStream());
			oos.flush();
			ois = new ObjectInputStream(socket.getInputStream());

		} catch (ConnectException ce) {
			ce.printStackTrace();
			setActive(false);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			setActive(false);
		} catch (IOException e) {
			e.printStackTrace();
			setActive(false);
		}
		System.out.println("Tentantiva de conexão " + this);
	}

	public boolean isBusy() {
		synchronized (busyLock) {
			return busy;
		}
	}

	public void setBusy(boolean busy) {
		synchronized (busyLock) {
			this.busy = busy;
		}
	}

	public boolean isActive() {
		synchronized (activeLock) {
			return active;
		}
	}

	public void setActive(boolean active) {
		synchronized (activeLock) {
			this.active = active;
		}
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public synchronized void initializeRangeQuery(float[] sample, double range) {

		if (socket == null) {
			return;
		}

		try {
			this.setBusy(true);

			oos.writeInt(ProtocolCode.INITIALRANGEQUERY.getProtocolCode());

//			oos.writeObject(sample);
			
			oos.writeInt(sample.length);
			for (int i = 0; i < sample.length; i++) {
				oos.writeFloat(sample[i]);
			}
			oos.writeDouble(range);

			oos.flush();

			this.notifyAll();
			
			inicializedServer = true;
			this.setBusy(false);
			// Não terá nenhuma resposta do servidor
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public synchronized Object partialRangeQuery(long blockId, double distance) {
		if (socket == null) {
			System.out.println("ERRO! Enviou requisicao para um host que não tem conexão");
			throw new RuntimeException("Host " + hostName + "não tem conexão. Reveja a lógica");
		}
		
		if (!inicializedServer) {
			try {
				this.wait();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		
		System.out.println("Enviando " + blockId);
		Object returnObject = null;
		try {

			this.setBusy(true);
			oos.writeInt(ProtocolCode.PARTIALRANGEQUERY.getProtocolCode());
			oos.writeLong(blockId);
			oos.writeDouble(distance);
			
			oos.flush();
//			Thread.sleep(300);
			
			try {
				returnObject = ois.readObject();
			
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
			this.setBusy(false);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnObject;
	}

	public QueryStats askForStats() {

		QueryStats stats = null;

		try {
			oos.writeInt(ProtocolCode.ASK_FOR_STATS.getProtocolCode());
			oos.flush();

			stats = (QueryStats) ois.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return stats;
	}

	public void closeConnection() {

		if (active) {
			try {
				this.oos.writeInt(ProtocolCode.CLOSECONNECTION.getProtocolCode());
				this.oos.close();
				this.ois.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public String toString() {
		String strReturn = hostName + " " + ((active) ? "Ativo" : "Inativo");
		return strReturn;
	}

}