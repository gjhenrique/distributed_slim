package br.uel.cross.parallel.mams.pagemanager;

import java.io.InputStream;
import java.io.OutputStream;

public interface Serializer<T> {
	
	public void write(OutputStream stream, T object);
	
	public T read(InputStream stream);
	
	
}
