package br.uel.cross.parallel.mams;

import java.io.IOException;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.model.IndexEntryTree;
import br.uel.cross.parallel.mams.model.IndexNodeTree;
import br.uel.cross.parallel.mams.model.LeafEntryTree;
import br.uel.cross.parallel.mams.model.LeafNodeTree;
import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.mams.network.ParallelRangeQuery;
import br.uel.cross.parallel.mams.pagemanager.PageManager;
import br.uel.cross.parallel.mams.result.PruningInfo;
import br.uel.cross.parallel.mams.result.TreeResult;
import br.uel.cross.parallel.mams.split.Promoter;
import br.uel.cross.parallel.mams.subtreechooser.SubTreeChooser;
import br.uel.cross.parallel.util.LogicNode;
import br.uel.cross.parallel.util.LogicNode.NodeType;
import br.uel.cross.parallel.util.NodeSimpleFactory;

<<<<<<< Updated upstream:src/main/java/br/uel/cross/parallel/mams/MAMTree.java
public class MAMTree {
		
	private NodeTree root;
=======
import java.io.IOException;
import java.util.*;

public class MamTree {
>>>>>>> Stashed changes:src/main/java/br/uel/cross/parallel/mams/MamTree.java

	private PageManager pageManager;

	private MetricEvaluator metricEvaluator;

	private Promoter promoter;

    private SubTreeChooser subtreeChooser;

<<<<<<< Updated upstream:src/main/java/br/uel/cross/parallel/mams/MAMTree.java
	private int maxOccupation;
=======
    private int maxOccupation;

    private int height;
    private int amount;
    private Set<Long> set = new TreeSet<Long>();

    public MamTree(PageManager pageManager, MetricEvaluator metricEvaluator, Promoter promoter,
                   SubTreeChooser subtreeChooser) {

        this.pageManager = pageManager;

        this.metricEvaluator = metricEvaluator;

        this.promoter = promoter;

        this.root = this.pageManager.findRoot();

        this.subtreeChooser = subtreeChooser;

        this.height = (this.root != null) ? this.pageManager.getTreeHeight() : 0;

    }

    public boolean add(float[] element, long rowId) {

        try {

            if (root == null) {

                root = this.pageManager.findRoot();

                root = NodeSimpleFactory.create(true);
                root.setBlockId(this.pageManager.getBlockId());

                LeafEntryTree entry = (LeafEntryTree) root.createEntry();

                entry.setSpaceElement(element);
                entry.setRepresentativeDistance(0);
                entry.setRowId(rowId);

                root.addEntry(entry);

                this.height++;

                this.pageManager.writeNode(root.getBlockId(), root);
                this.pageManager.changeRootNode(root.getBlockId(), 1);

            } else {
                SubTreeInfo promo1 = new SubTreeInfo();
                SubTreeInfo promo2 = new SubTreeInfo();

                if (insertRecursive(root, element, null, promo1, promo2, rowId) == InsertAction.PROMOTION) {

                    addNewRoot(promo1, promo2);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public InsertAction insertRecursive(NodeTree node, float[] element,
                                        float[] representativeElement, SubTreeInfo promo1, SubTreeInfo promo2, long rowId)
            throws IOException {
>>>>>>> Stashed changes:src/main/java/br/uel/cross/parallel/mams/MamTree.java

	private int height;

	enum InsertAction {
		NO_ACT, CHANGE_REP, PROMOTION
	}

	public MAMTree(PageManager pageManager, MetricEvaluator metricEvaluator, Promoter promoter,
			SubTreeChooser subtreeChooser) {

		this.pageManager = pageManager;

		this.metricEvaluator = metricEvaluator;

		this.promoter = promoter;

		this.root = this.pageManager.findRoot();

		this.subtreeChooser = subtreeChooser;

		this.height = (this.root != null) ? this.pageManager.getTreeHeight() : 0;

	}

	public boolean add(float[] element, long rowId) {

		try {

			if (root == null) {

				root = this.pageManager.findRoot();
				
				root = NodeSimpleFactory.create(true);
				root.setBlockId(this.pageManager.getBlockId());

				LeafEntryTree entry = (LeafEntryTree) root.createEntry();

				entry.setSpaceElement(element);
				entry.setRepresentativeDistance(0);
				entry.setRowId(rowId);

				root.addEntry(entry);
				
				this.height++;
				
				this.pageManager.writeNode(root.getBlockId(), root);
				this.pageManager.changeRootNode(root.getBlockId(), 1);

			} else {
				SubTreeInfo promo1 = new SubTreeInfo();
				SubTreeInfo promo2 = new SubTreeInfo();
								
				if (insertRecursive(root, element, null, promo1, promo2, rowId) == InsertAction.PROMOTION) {

					addNewRoot(promo1, promo2);
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public InsertAction insertRecursive(NodeTree node, float[] element,
			float[] representativeElement, SubTreeInfo promo1, SubTreeInfo promo2, long rowId)
					throws IOException {

		InsertAction result = null;

		if (node instanceof IndexNodeTree) {
			// Nó índice
			IndexNodeTree indexNode = (IndexNodeTree) node;
			
			int subtreeIndex = this.subtreeChooser.subtreeChooser(indexNode, element, this.metricEvaluator);

			float[] subRep = indexNode.getEntry().get(subtreeIndex).getSpaceElement();
						
			IndexEntryTree chosenNode = (IndexEntryTree) indexNode.getEntry().get(subtreeIndex);

			NodeTree ptrNode = this.pageManager.fromFileToNode(chosenNode.getPtr());

			IndexEntryTree indexEntry = null;

			boolean fitNode = false;

			switch (this.insertRecursive(ptrNode, element, subRep, promo1, promo2, rowId)) {

			case NO_ACT:

				indexEntry = (IndexEntryTree) indexNode.getEntry().get(subtreeIndex);

				int updatednEntries = indexEntry.getnEntries() + 1;

				indexEntry.setnEntries(updatednEntries);
				indexEntry.setConveringRadius(promo1.getRadius());

				promo1.setnObjects(indexNode.getTotalObjectCount());
				promo1.setRadius(indexNode.getMinimumRadius());

				this.pageManager.writeNode(indexNode.getBlockId(), indexNode);

				result = InsertAction.NO_ACT;

				break;
			case CHANGE_REP:
				// Remove previous entry.
				indexNode.getEntry().remove(subtreeIndex);

				// Remove previous
				indexEntry = (IndexEntryTree) indexNode.createEntry();

				indexEntry.setConveringRadius(promo1.getRadius());
				indexEntry.setnEntries(promo1.getnObjects());
				indexEntry.setPtr(promo1.getRootId());
				indexEntry.setSpaceElement(promo1.getRep());

				// Try to add the new entry...
				fitNode = indexNode.addEntry(indexEntry);

				if (fitNode) {
					// Swap OK. Fill data.

					// Will it replace the representative ?
					// WARNING: Do not change the order of this checking.
					if ((representativeElement != null) && (representativeElement.equals(subRep))) {

						indexEntry.setRepresentativeDistance(0);

						// Oops! We must propagate the representative change
						// promo1.Rep will remain the same.
						promo1.setRootId(indexNode.getBlockId());

						updateDistances(indexNode, representativeElement, indexNode.getNumberOfEntries() - 1);

						result = InsertAction.CHANGE_REP;
					} else {
						// promo1.Rep is not the new representative.
						if (representativeElement != null) {
							// Distance from representative is...
							double distance = this.metricEvaluator.getDistance(representativeElement,
									promo1.getRep());

							indexEntry.setRepresentativeDistance(distance);
						} else {
							// It is the root!
							indexEntry.setRepresentativeDistance(0);
						}

						promo1.setRep(null);
						result = InsertAction.NO_ACT;
					}
					promo1.setRadius(indexNode.getMinimumRadius());
					promo1.setnObjects(promo1.getnObjects());
				} else {
					// Split it!
					// New node.
					long newBlockId = this.pageManager.getBlockId();
					IndexNodeTree newIndexNode = new IndexNodeTree(newBlockId);

					splitIndex(indexNode, newIndexNode, promo1.getRep(), promo1.getRadius(),
							promo1.getRootId(), promo1.getnObjects(), null, 0, 0, 0, representativeElement,
							promo1, promo2);

					this.pageManager.writeNode(newIndexNode.getBlockId(), newIndexNode);

					result = InsertAction.PROMOTION;
				}

				this.pageManager.writeNode(indexNode.getBlockId(), indexNode);

				break;
			case PROMOTION:

				// Update subtree
				if (promo1.getRep() == null) {

					indexEntry = (IndexEntryTree) indexNode.getEntry().get(subtreeIndex);

					indexEntry.setnEntries(promo1.getnObjects());
					indexEntry.setConveringRadius(promo1.getRadius());
					indexEntry.setPtr(promo1.getRootId());

					IndexEntryTree entryPromo2 = (IndexEntryTree) indexNode.createEntry();

					entryPromo2.setnEntries(promo2.getnObjects());
					entryPromo2.setConveringRadius(promo2.getRadius());
					entryPromo2.setPtr(promo2.getRootId());
					entryPromo2.setSpaceElement(promo2.getRep());

					fitNode = indexNode.addEntry(entryPromo2);

					if (fitNode) {

						// Swap OK. Fill data.
						if (representativeElement != null) {
							// Distance from representative is...
							double distance = this.metricEvaluator.getDistance(representativeElement,
									promo2.getRep());
							entryPromo2.setRepresentativeDistance(distance);
						} else {
							// It is the root!
							entryPromo2.setRepresentativeDistance(0);
						}

						promo1.setRadius(indexNode.getMinimumRadius());
						promo1.setnObjects(indexNode.getTotalObjectCount());

						result = InsertAction.NO_ACT;
					} else {

						// Split it!
						// New node.
						IndexNodeTree newIndexNode = new IndexNodeTree();
						newIndexNode.setBlockId(this.pageManager.getBlockId());

						splitIndex(indexNode, newIndexNode, promo2.getRep(), promo2.getRadius(),
								promo2.getRootId(), promo2.getnObjects(), null, 0, 0, 0,
								representativeElement, promo1, promo2);

						this.pageManager.writeNode(newIndexNode.getBlockId(), newIndexNode);

						result = InsertAction.PROMOTION;
					}

				} else {
					// Remove the previous entry
					indexNode.getEntry().remove(subtreeIndex);

					IndexEntryTree newIndexEntry = (IndexEntryTree) indexNode.createEntry();

					newIndexEntry.setConveringRadius(promo1.getRadius());
					newIndexEntry.setnEntries(promo1.getnObjects());
					newIndexEntry.setPtr(promo1.getRootId());
					newIndexEntry.setSpaceElement(promo1.getRep());

					fitNode = indexNode.addEntry(newIndexEntry);

					if (fitNode) {
						// Will it replace the representative ?
						if ((representativeElement != null) && (representativeElement.equals(subRep))) {
							// promo1.Rep is the new representative.
							newIndexEntry.setRepresentativeDistance(0);
							promo1.setRootId(node.getBlockId());

							this.updateDistances(indexNode, promo1.getRep(), indexNode.getEntry().size() - 1);

							result = InsertAction.CHANGE_REP;
						} else {
							// promo1.Rep is not the new representative.
							if (representativeElement != null) {
								// Distance from representative is...
								newIndexEntry.setRepresentativeDistance(this.metricEvaluator.getDistance(
										representativeElement, promo1.getRep()));
							} else {
								// It is the root!
								newIndexEntry.setRepresentativeDistance(0.0);
							}
							result = InsertAction.NO_ACT;
						}

						// Try to add promo2
						IndexEntryTree newIndexEntry2 = (IndexEntryTree) indexNode.createEntry();
						newIndexEntry2.setConveringRadius(promo2.getRadius());
						newIndexEntry2.setnEntries(promo2.getnObjects());
						newIndexEntry2.setPtr(promo2.getRootId());
						newIndexEntry2.setSpaceElement(promo2.getRep());

						boolean fitNode2 = indexNode.addEntry(newIndexEntry2);

						if (fitNode2) {
							// Swap OK. Fill data.
							if (promo1.getRep() != null) {
								// Rep. changed...
								// Distance from representative is...
								double distance = this.metricEvaluator.getDistance(promo1.getRep(),
										promo2.getRep());
								newIndexEntry2.setRepresentativeDistance(distance);
							} else {
								// No change!
								if (representativeElement != null) {
									// Distance from representative is...
									double distance = this.metricEvaluator.getDistance(representativeElement,
											promo2.getRep());
									newIndexEntry2.setRepresentativeDistance(distance);
								} else {
									// It is the root!
									newIndexEntry2.setRepresentativeDistance(0);
									;
								}
							}

							promo2.setRep(null);
							promo1.setnObjects(indexNode.getTotalObjectCount());
							promo1.setRadius(indexNode.getMinimumRadius());
						} else {
							// Split it promo2.rep does not fit.
							// New node.
							long blockId = this.pageManager.getBlockId();
							IndexNodeTree indexNode2 = new IndexNodeTree(blockId);

							// Dispose promo1.rep it if exists because it will
							// not be
							// used again. It happens when result is CHANGE_REP.
							if (promo1.getRep() != null) {
								promo1.setRep(null);
							}

							splitIndex(indexNode, indexNode2, promo2.getRep(), promo2.getRadius(),
									promo2.getRootId(), promo2.getnObjects(), null, 0, 0, 0,
									representativeElement, promo1, promo2);

							this.pageManager.writeNode(indexNode2.getBlockId(), indexNode2);
							result = InsertAction.PROMOTION;
						}
					} else {
						// Split it because both objects don't fit.
						// New node.
						long newBlockId = this.pageManager.getBlockId();
						IndexNodeTree newIndexNode = new IndexNodeTree(newBlockId);

						splitIndex(indexNode, newIndexNode, promo1.getRep(), promo1.getRadius(),
								promo1.getRootId(), promo1.getnObjects(), promo2.getRep(),
								promo2.getRadius(), promo2.getRootId(), promo2.getnObjects(),
								representativeElement, promo1, promo2);

						this.pageManager.writeNode(newIndexNode.getBlockId(), newIndexNode);
				        result = InsertAction.PROMOTION; //Report split.

					}
				}
				this.pageManager.writeNode(indexNode.getBlockId(), indexNode);
				break;
			default:
				System.err.println("Saída de erro");
				System.exit(1);
				break;
			}
		} else {
			// Inserir novo elemento no nó folha
			LeafNodeTree leafNode = (LeafNodeTree) node;

			LeafEntryTree newEntry = (LeafEntryTree) leafNode.createEntry();
			newEntry.setSpaceElement(element);
			newEntry.setRowId(rowId);

			boolean isEntryFitNode = leafNode.addEntry(newEntry);

			if (isEntryFitNode) {
				// Novo elemento coube no nó folha

				double dist = 0;
				if (representativeElement != null) {
					dist = metricEvaluator.getDistance(element, representativeElement);
				}

				newEntry.setRepresentativeDistance(dist);

				promo1.setRep(null);
				promo1.setRadius(leafNode.getMinimumRadius());
				promo1.setRootId(leafNode.getBlockId());
				promo1.setnObjects(leafNode.getNumberOfEntries());

				result = InsertAction.NO_ACT;
			} else {
				// Realizar o Split

				long newBlockId = this.pageManager.getBlockId();
				LeafNodeTree newLeafNode = new LeafNodeTree(newBlockId);
				newLeafNode.setBlockId(newBlockId);

				newEntry.setRowId(rowId);
				this.splitLeaf(node, newLeafNode, newEntry, representativeElement, promo1, promo2);

				this.pageManager.writeNode(newLeafNode.getBlockId(), newLeafNode);

				result = InsertAction.PROMOTION;
			}

			this.pageManager.writeNode(leafNode.getBlockId(), leafNode);
		}
		return result;
	}

	private void splitIndex(IndexNodeTree oldNode, IndexNodeTree newNode, float[] newObj1,
			double newRadius1, long newNodeID1, int newNEntries1, float[] newObj2, double newRadius2,
			long newnodeID2, int newNEntries2, float[] prevRep, SubTreeInfo promo1, SubTreeInfo promo2) {
		
		int numberOfEntries = oldNode.getNumberOfEntries();

		LogicNode logicNode = new LogicNode(numberOfEntries + 2);
		logicNode.setNodeType(NodeType.INDEXNODE);
		logicNode.addIndexNode(oldNode);

		int idx = logicNode.addEntry(newObj1);

		logicNode.setEntry(idx, newNodeID1, newNEntries1, newRadius1, 0);

		if (newObj2 != null) {
			logicNode.addEntry(newObj2);
			logicNode.setEntry(logicNode.getCount(), newnodeID2, newNEntries2, newRadius2, 0);
		}

		this.manageSplit(logicNode, oldNode, newNode, promo1, promo2, prevRep);
	}

	private void splitLeaf(NodeTree oldNode, NodeTree newNode, LeafEntryTree newEntry, float[] prevRep,
			SubTreeInfo promo1, SubTreeInfo promo2) {

		int numberOfEntries = oldNode.getNumberOfEntries();

		LogicNode logicNode = new LogicNode(numberOfEntries + 1);

		logicNode.setNodeType(NodeType.LEAFNODE);
		logicNode.addLeafNode(oldNode);

		logicNode.addEntry(newEntry.getSpaceElement());
		logicNode.setEntry(logicNode.getCount() - 1, 0, 0, 0, newEntry.getRowId());

		this.setMaxOccupation(numberOfEntries);

		this.manageSplit(logicNode, oldNode, newNode, promo1, promo2, prevRep);
	}

	private void manageSplit(LogicNode logicNode, NodeTree oldNode, NodeTree newNode, SubTreeInfo promo1,
			SubTreeInfo promo2, float[] prevRep) {

		float[] lRep = null;
		float[] rRep = null;
		this.promoter.promote(logicNode, this.metricEvaluator);

		oldNode.removeAll();

		this.promoter.distribute(logicNode, oldNode, lRep, newNode, rRep, this.metricEvaluator);
		
		int dimensions = logicNode.getRepresentativeElement(0).length;
		
    	lRep = Arrays.copyOf(logicNode.getRepresentativeElement(0), dimensions);
    	rRep = Arrays.copyOf(logicNode.getRepresentativeElement(1), dimensions);
		
		logicNode = null;

		if (prevRep == null) {
			// Nó pai é raiz
			setPromoProperties(promo1, oldNode, lRep);
			setPromoProperties(promo2, newNode, rRep);
		} else {

			if (prevRep.equals(lRep)) {

				lRep = null;
				setPromoProperties(promo1, oldNode, null);
				setPromoProperties(promo2, newNode, rRep);

			} else if (prevRep.equals(rRep)) {

				rRep = null;
				setPromoProperties(promo2, oldNode, lRep);
				setPromoProperties(promo1, newNode, null);

			} else {

				setPromoProperties(promo1, oldNode, lRep);
				setPromoProperties(promo2, newNode, rRep);
			}
		}
	}

	private void setPromoProperties(SubTreeInfo promo, NodeTree node, float[] rep) {
		promo.setRep(rep);
		promo.setRadius(node.getMinimumRadius());
		promo.setRootId(node.getBlockId());
		promo.setnObjects(node.getTotalObjectCount());

	}

	private void addNewRoot(SubTreeInfo promo1, SubTreeInfo promo2) {

		long block = this.pageManager.getBlockId();

		IndexNodeTree newRoot = new IndexNodeTree(block);
				
		IndexEntryTree entry1 = new IndexEntryTree(promo1.getnObjects(), promo1.getRootId(),
				promo1.getRadius(), promo1.getRep(), 0);
		IndexEntryTree entry2 = new IndexEntryTree(promo2.getnObjects(), promo2.getRootId(),
				promo2.getRadius(), promo2.getRep(), 0);

		newRoot.addEntry(entry1);
		newRoot.addEntry(entry2);

		this.height++; 
		
		this.pageManager.changeRootNode(newRoot.getBlockId(), this.height);
		
		this.root = newRoot;

		System.out.println("Nova Raiz com id " + block + " e altura " + height);
		
		this.pageManager.writeNode(newRoot.getBlockId(), newRoot);
	}

	public void setMaxOccupation(int newValue) {
		if (this.maxOccupation < newValue) {
			this.maxOccupation = newValue;
		}
	}

	public void updateDistances(IndexNodeTree node, float[] repObj, int repObjIdx) {

		double distance;
		for (int i = 0; i < node.getNumberOfEntries(); i++) {
			if (i != repObjIdx) {

				EntryTree entry = node.getEntry().get(i);
				distance = this.metricEvaluator.getDistance(repObj, entry.getSpaceElement());
				entry.setRepresentativeDistance(distance);
			}
			node.getEntry().get(i).setRepresentativeDistance(0.0);
		}
	}

	
	private int amount;
	private Set<Long> set = new TreeSet<Long>();
	public TreeResult search(NodeTree nodeTree, long represId, int height) {

		TreeResult result = new TreeResult();
		
		if (nodeTree == null) {
			nodeTree = this.root;
		}		
		
		if(height == 2){
			System.out.println(represId + "  => " + nodeTree.getBlockId());
			amount++;
			set.add(represId);
			System.out.println(amount/set.size() + "  " + set.size());
		}
		height++;
		
		if (nodeTree instanceof IndexNodeTree) {
			
			for (EntryTree entry : nodeTree.getEntry()) {
				
				IndexEntryTree internalEntry = (IndexEntryTree) entry;
				
				search(this.pageManager.fromFileToNode(internalEntry.getPtr()), nodeTree.getBlockId(), height);
			}

		} else if (nodeTree instanceof LeafNodeTree) {
			
			for (EntryTree entry : nodeTree.getEntry()) {
				
				LeafEntryTree leafEntry = (LeafEntryTree) entry;
				result.addPair(leafEntry.getRowId(), entry.getRepresentativeDistance());
			}
		}

		return result;
	}

	public TreeResult simpleRangeQuery(float[] sample, double range) {

		TreeResult result = new TreeResult();

		double distance;

		result.setQueryInfoRangeQuery(sample, range);
				
		// Não existe raiz
		if (this.root != null) {

			if (this.root instanceof IndexNodeTree) {

				for (EntryTree entry : this.root.getEntry()) {

					IndexEntryTree internalEntry = (IndexEntryTree) entry;

					distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);
					
					//System.out.println("Raio de cobertura raiz => " + internalEntry.getConveringRadius());
					
					if (distance <= range + internalEntry.getConveringRadius()) {
						this.simpleRangeQueryRecursive(internalEntry.getPtr(), result, sample, range,
								distance);
					}
				}
			} else {
				for (EntryTree entry : this.root.getEntry()) {

					LeafEntryTree leafEntry = (LeafEntryTree) entry;

					distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);
					if (distance <= range) {
						result.addPair(leafEntry.getRowId(), distance);
					}
				}
			}
		}
		else{
			throw new RuntimeException("Nao existe raiz");
		}
		return result;
	}

	private void simpleRangeQueryRecursive(long nodeId, TreeResult result, float[] sample, double range,
			double representativeDistance) {

		NodeTree node = this.pageManager.fromFileToNode(nodeId);
		
		double distance;
		if (node != null) {

			if (node instanceof IndexNodeTree) {

				for (EntryTree entry : node.getEntry()) {
					IndexEntryTree indexEntry = (IndexEntryTree) entry;
										
					if (Math.abs(representativeDistance - indexEntry.getRepresentativeDistance()) <= range
							+ indexEntry.getConveringRadius()) {
						distance = this.metricEvaluator.getDistance(indexEntry.getSpaceElement(), sample);

						if (distance <= range + indexEntry.getConveringRadius()) {

							this.simpleRangeQueryRecursive(indexEntry.getPtr(), result, sample, range,
									distance);
						}
					}
				}
			} else {

				for (EntryTree entry : node.getEntry()) {

					if (Math.abs(representativeDistance - entry.getRepresentativeDistance()) <= range) {

						distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);

						LeafEntryTree leafEntry = (LeafEntryTree) entry;
						if (distance <= range) {
							result.addPair(leafEntry.getRowId(), distance);
						}
					}
				}
			}
		}
	}

//	Repeticao de código da RQ normal
//	Nao sei como abstrair essa consulta 
	public PruningInfo simpleRangeQueryWithPruningInfo(float[] sample, double range) {

		PruningInfo info = new PruningInfo(this.pageManager.getTreeHeight());
		
		double distance;
		
		int height = 1;
		// Não existe raiz
		if (this.root != null) {

			if (this.root instanceof IndexNodeTree) {
				
				for (EntryTree entry : this.root.getEntry()) {

					IndexEntryTree internalEntry = (IndexEntryTree) entry;

					distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);
					
					//System.out.println("Raio de cobertura raiz => " + internalEntry.getConveringRadius());
					
					if (distance <= range + internalEntry.getConveringRadius()) {
						info.addIndexNonPruning(height);
						this.simpleRangeQueryRecursiveWithPruningInfo(internalEntry.getPtr(), info, sample, range,
								distance, height);
					}
					else
						info.addIndexPruning(height);
				}
			} else {
				for (EntryTree entry : this.root.getEntry()) {

					distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);
					if (distance <= range) {
						info.addLeafNonPruning(height);
					}
					else{
						info.addLeafNonPruning(height);
					}
				}
			}
		}
		else{
			throw new RuntimeException("Nao existe raiz");
		}
		return info;
	}

	private void simpleRangeQueryRecursiveWithPruningInfo(long nodeId, PruningInfo info, float[] sample, double range,
			double representativeDistance, int height) {

		NodeTree node = this.pageManager.fromFileToNode(nodeId);
		
		double distance;
		if (node != null) {
			
			height++;
			if (node instanceof IndexNodeTree) {
				for (EntryTree entry : node.getEntry()) {
					IndexEntryTree indexEntry = (IndexEntryTree) entry;
										
					if (Math.abs(representativeDistance - indexEntry.getRepresentativeDistance()) <= range
							+ indexEntry.getConveringRadius()) {
						distance = this.metricEvaluator.getDistance(indexEntry.getSpaceElement(), sample);

						if (distance <= range + indexEntry.getConveringRadius()) {
							info.addIndexNonPruning(height);
							this.simpleRangeQueryRecursiveWithPruningInfo(indexEntry.getPtr(), info, sample, range,
									distance, height);
						}
						else
							info.addIndexPruning(height);
					}
					else 
						info.addIndexPruning(height);
				}
			} else {

				for (EntryTree entry : node.getEntry()) {

					if (Math.abs(representativeDistance - entry.getRepresentativeDistance()) <= range) {

						distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);

						if (distance <= range)
							info.addLeafNonPruning(height);
						else
							info.addLeafPruning(height);
					}
					else{
						info.addLeafPruning(height);
					}
				}
			}
		}
	}
	
	public void parallelRangeQuery(float[] sample, double range,
			ParallelRangeQuery rangeQuery) {
		
		if (this.root instanceof IndexNodeTree) {

			for (EntryTree entry : this.root.getEntry()) {

				IndexEntryTree internalEntry = (IndexEntryTree) entry;

				double distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);

				if (distance <= range + internalEntry.getConveringRadius()) {

					rangeQuery.parallelRangeQuery(internalEntry.getPtr(), distance);
				}
			}
		} else {
			throw new RuntimeException("O nó raíz é folha!!!. Não é necessário rodar a consulta paralela");
		}
	}

	public TreeResult nearestQuery(float[] sample, int k, boolean tie) {

		double distanceRepres = 0;
		double distance = 0;

		TreeResult result = new TreeResult();
		result.setQueryInfoKnn(sample, k, Double.MAX_VALUE, tie);

		double rangeK = Double.MAX_VALUE;

		if (this.root == null) {
			throw new RuntimeException("Raiz é nula");
		}

		Queue<PriorityQueueValue> queue = new PriorityQueue<PriorityQueueValue>();

		// Incializando com a raiz atual
		PriorityQueueValue currentValue = new PriorityQueueValue(this.root.getBlockId(), 0, 0);

		while (currentValue != null) {

			NodeTree currentNode = this.pageManager.fromFileToNode(currentValue.pageId);

			if (currentNode instanceof IndexNodeTree) {

				IndexNodeTree indexNode = (IndexNodeTree) currentNode;

				for (int i = 0; i < indexNode.getNumberOfEntries(); i++) {

					IndexEntryTree entry = (IndexEntryTree) indexNode.getEntry(i);

					if (Math.abs(distanceRepres - entry.getRepresentativeDistance()) <= rangeK
							+ entry.getConveringRadius()) {

						distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);

						if (distance <= rangeK + entry.getConveringRadius()) {

							PriorityQueueValue value = new PriorityQueueValue(entry.getPtr(),
									entry.getConveringRadius(), distance);
							queue.add(value);
						}
					}
				}
			} else {

				LeafNodeTree leafNode = (LeafNodeTree) currentNode;

				for (int i = 0; i < leafNode.getNumberOfEntries(); i++) {

					LeafEntryTree entry = (LeafEntryTree) leafNode.getEntry(i);

					if (Math.abs(distanceRepres - entry.getRepresentativeDistance()) <= rangeK) {

						distance = this.metricEvaluator.getDistance(entry.getSpaceElement(), sample);

						if (distance <= rangeK) {

							result.addPair(entry.getRowId(), distance);

							if (result.getNumberOfEntries() >= k) {
								result.cut(k);
								rangeK = result.getMaximumDistance();
							}
						}
					}
				}
			}

			boolean stop = false;

			do{
				currentValue = queue.poll();

				if(currentValue != null){
						
					if(distance <= rangeK + currentValue.radius) {
						distanceRepres = distance;
						stop = true;
					}
				}
				else{
					currentValue = null;
					stop = true;
				}
			} while(!stop);
		}

		return result;
	}

	private class PriorityQueueValue implements Comparable<PriorityQueueValue> {

		public long pageId;
		public double radius;
		public double distance;

		public PriorityQueueValue(long pageId, double radius, double distance) {
			this.pageId = pageId;
			this.radius = radius;
			this.distance = distance;
		}

		@Override
		public int compareTo(PriorityQueueValue o) {
			if (this.distance == o.distance)
				return -1;

			return (int) -(this.distance - o.distance);
		}
	}

    public Promoter getPromoter() {
        return promoter;
    }

    public PageManager getPageManager() {
        return pageManager;
    }

    public MetricEvaluator getMetricEvaluator() {
        return metricEvaluator;
    }

    public SubTreeChooser getSubtreeChooser() {
        return subtreeChooser;
    }

}