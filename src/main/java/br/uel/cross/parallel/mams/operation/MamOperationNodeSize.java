package br.uel.cross.parallel.mams.operation;

<<<<<<< Updated upstream
import java.util.ArrayList;
import java.util.List;

import br.uel.cross.parallel.mams.MAMTree;
=======
import br.uel.cross.parallel.mams.MamTree;
>>>>>>> Stashed changes
import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.model.IndexEntryTree;
import br.uel.cross.parallel.mams.model.LeafEntryTree;
import br.uel.cross.parallel.util.EntrySizeMeasurer;

public class MamOperationNodeSize extends MamOperation {

<<<<<<< Updated upstream
	private float[] points;
=======
    private float[] points;

    public MamOperationNodeSize(MamTree mamTree) {
        super(mamTree);
    }

    @Override
    protected void performActionMam(float[] points) {

        if (queryNumber >= 1) {
            return;
        }

        this.points = points;
        this.registerIndexNode(new ArrayList<EntryTree>());
        this.registerLeafNode(new ArrayList<EntryTree>());
    }
>>>>>>> Stashed changes

	public MamOperationNodeSize(MAMTree mamTree) {
		super(mamTree);
	}

	@Override
	protected void performActionMam(float[] points) {

		if (queryNumber >= 1) {
			return;
		}

		this.points = points;
		this.registerIndexNode(new ArrayList<EntryTree>());
		this.registerLeafNode(new ArrayList<EntryTree>());
	}

	private void registerIndexNode(List<EntryTree> entries) {

		this.populateEntryList(entries, new IndexEntryTree());
		this.populateEntryList(entries, new IndexEntryTree());

		EntrySizeMeasurer.getNumberOfIndexEntries(entries);
	}

	private void registerLeafNode(List<EntryTree> entries) {

		this.populateEntryList(entries, new LeafEntryTree());
		this.populateEntryList(entries, new LeafEntryTree());

		EntrySizeMeasurer.getNumberOfLeafEntries(entries);
	}

	private void populateEntryList(List<EntryTree> entries, EntryTree entry) {
		entry.setSpaceElement(points);
		entries.add(entry);
	}
	
	public String calculateNumberOfNodes(int numberOfEntries, int height){
		
		StringBuilder builder = new StringBuilder();

		for (int i = 1; i <= height; i++) {
			builder.append(i + "  => " + (long) Math.pow(numberOfEntries, i) + "\n");
		}
	
		return builder.toString();
	}

	public String calculateSpace(int numberOfEntries, int blockSize, int height){
		StringBuilder builder = new StringBuilder();

		for (int i = 1; i <= height; i++) {
			builder.append(i + "  => " + (Math.pow(numberOfEntries, i)/numberOfEntries) * blockSize / 1000000.0 + "\n");
		}
	
		return builder.toString();
	}
}
