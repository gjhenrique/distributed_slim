package br.uel.cross.parallel.mams.statiscs;

public enum StatsAttributeType {
	COUNTER, AVERAGE;
}
