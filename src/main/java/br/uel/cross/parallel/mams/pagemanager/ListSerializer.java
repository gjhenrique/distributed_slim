package br.uel.cross.parallel.mams.pagemanager;

import java.util.ArrayList;

import br.uel.cross.parallel.mams.model.EntryTree;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class ListSerializer extends com.esotericsoftware.kryo.Serializer<ArrayList<EntryTree>> {
	
	EntryTreeSerializer serializer = new EntryTreeSerializer();
	
	@Override
	public void write(Kryo kryo, Output output, ArrayList<EntryTree> object) {

		output.writeInt(object.size());

		for (EntryTree entry : object) {
			kryo.writeObject(output, entry, serializer);
		}

	}

	@Override
	public synchronized ArrayList<EntryTree> read(Kryo kryo, Input input, Class<ArrayList<EntryTree>> type) {

		int elementsSize = input.readInt();

		ArrayList<EntryTree> listEntry = new ArrayList<EntryTree>(elementsSize);

		int i = 0;
		while (i < elementsSize) {
			EntryTree object = kryo.readObject(input, EntryTree.class);
			listEntry.add(object);
			i++;
		}

		return listEntry;
	}

}
