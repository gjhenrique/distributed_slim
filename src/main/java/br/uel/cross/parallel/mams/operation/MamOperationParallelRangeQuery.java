package br.uel.cross.parallel.mams.operation;

<<<<<<< Updated upstream
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import br.uel.cross.parallel.mams.MAMTree;
=======
import br.uel.cross.parallel.mams.MamTree;
>>>>>>> Stashed changes
import br.uel.cross.parallel.mams.network.ParallelRangeQuery;
import br.uel.cross.parallel.mams.network.ParallelSearchManager;
import br.uel.cross.parallel.mams.network.QueryResultCallback;
import br.uel.cross.parallel.mams.result.TreeResult;
import br.uel.cross.parallel.mams.statiscs.QueryStats;

public class MamOperationParallelRangeQuery extends MamOperation implements QueryResultCallback {

<<<<<<< Updated upstream
	private ParallelSearchManager searchManager;
=======
    private ParallelSearchManager searchManager;

    private double range;

    public MamOperationParallelRangeQuery(MamTree mamTree, ParallelSearchManager searchManager, double range) {
        super(mamTree);

        this.range = range;

        this.searchManager = searchManager;
    }
>>>>>>> Stashed changes

	private double range;

	public MamOperationParallelRangeQuery(MAMTree mamTree, ParallelSearchManager searchManager, double range) {
		super(mamTree);

		this.range = range;

		this.searchManager = searchManager;
	}

	@Override
	protected void performActionMam(float[] points) {
		
		String identifier = Integer.toString(queryNumber);
		
		ParallelRangeQuery rangeQuery = this.searchManager
				.startNewRangeQuery(points, range, identifier, this);

		this.tree.parallelRangeQuery(points, range, rangeQuery);
	}

	public String getIdentifier(){
		return queryNumber+"";
	}
	
//	Aspecto cuidará
	@Override
	public void handleQueryResult(TreeResult result, String identifier, List<QueryStats> stats) {
		
		System.out.println("Algoritmo terminou. Escrevendo estatisticas no arquivo");

	}

}
