package br.uel.cross.parallel.mams.pagemanager;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class JavaSerializer<T> implements Serializer<T> {

	@Override
	public void write(OutputStream stream, T object) {
		
		ObjectOutputStream oos = null;
		
		try {
			oos = new ObjectOutputStream(stream);
			oos.writeObject(object);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T read(InputStream stream) {

		ObjectInputStream ois = null;
		T object = null;
		try {
			ois = new ObjectInputStream(stream);

			object = (T) ois.readObject();
			
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		
		return object;
	}

}
