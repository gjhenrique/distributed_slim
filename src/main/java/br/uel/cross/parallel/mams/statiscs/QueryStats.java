package br.uel.cross.parallel.mams.statiscs;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import br.uel.cross.parallel.mams.result.TreeResult;
import br.uel.cross.parallel.util.TimeFormatter;

public class QueryStats implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5986997660647376448L;

	private long bigger = 0;
	private long minor = Long.MAX_VALUE;

	private long startGlobalTime;
	private long endGlobalTime;

	private ConcurrentMap<String, Long> statsQueryDuration = new ConcurrentHashMap<String, Long>();
	private ConcurrentMap<String, Integer> statsQueryElements = new ConcurrentHashMap<String, Integer>();

	private ConcurrentMap<String, StatsAttribute> statsHosts = new ConcurrentHashMap<String, StatsAttribute>();

	private String fileName;

	private String hostName;

	private StatsAttribute attributeStats = new StatsAttribute();

	private void writeElements(StringBuilder builder) {

		for (Map.Entry<String, Long> entry : statsQueryDuration.entrySet()) {
			builder.append("\n" + entry.getKey() + " => " + TimeFormatter.formattedTime(entry.getValue())
					+ "\t" + statsQueryElements.get(entry.getKey()) + " elementos");
		}
		builder.append("\n\n" + statsQueryElements.size() + " Consultas");
	}

	private void writeTime(StringBuilder builder) {
		builder.append("\n\nMaior => " + TimeFormatter.formattedTime(bigger))
				.append("\nMenor => " + TimeFormatter.formattedTime(minor))
				.append("\nMédia => " + TimeFormatter.formattedTime(average()))
				.append("\nTempo Total => " + TimeFormatter.formattedTime(startGlobalTime, endGlobalTime));
	}

	public void startGlobalTime() {
		this.startGlobalTime = System.nanoTime();
	}

	public void finishGlobalTime() {
		this.endGlobalTime = System.nanoTime();
	}

	public void addResut(String identifier, long durationTime, TreeResult result) {

		this.checkIfBigger(durationTime);
		this.checkIfMinor(durationTime);

		statsQueryDuration.put(identifier, durationTime);
		statsQueryElements.put(identifier, result.getNumberOfEntries());
	}

	private void checkIfMinor(long durationTime) {

		if (durationTime < minor) {
			this.minor = durationTime;
		}
	}

	private void checkIfBigger(long durationTime) {

		if (durationTime > bigger) {
			this.bigger = durationTime;
		}
	}

	private long average() {

		long avg = 0;

		for (Map.Entry<String, Long> entry : statsQueryDuration.entrySet()) {
			avg += entry.getValue();
		}

		return (statsQueryDuration.size() == 0) ? 0 : avg / statsQueryDuration.size();
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void mergeStats(QueryStats stats) {

		StatsAttribute hostAttribute = this.getStatsHost(stats.getHostName());

		for (StatsType type : StatsType.values()) {
			long value = stats.attributeStats.getStatsAttribute(type);

			if (type.getType() == StatsAttributeType.COUNTER) {
				this.attributeStats.addStats(type, value);
				hostAttribute.addStats(type, value);
			} else {
				this.attributeStats.averageStats(type, value);
				hostAttribute.averageStats(type, value);
			}
		}

	}

	private StatsAttribute getStatsHost(String hostName) {

		StatsAttribute attribute = statsHosts.get(hostName);

		if (attribute == null) {

			attribute = new StatsAttribute();
			statsHosts.put(hostName, attribute);

		}

		return attribute;
	}

	public void incrementStats(StatsType type) {
		this.attributeStats.incrementStats(type);
	}

	public void averageStats(StatsType type, long value) {
		this.attributeStats.averageStats(type, value);
	}

	public void setStats(StatsType type, long value) {
		this.attributeStats.setStats(type, value);
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();

		builder.append(TimeFormatter.now());

		this.writeElements(builder);
		this.writeTime(builder);
		builder.append(attributeStats);

		return builder.toString();

	}

	public String getStringHostAttributes() {

		StringBuilder builder = new StringBuilder();

		for (Map.Entry<String, StatsAttribute> entry : statsHosts.entrySet()) {

			builder.append("\n\n" + entry.getKey());

			builder.append("\n" + entry.getValue());

		}

		return builder.toString();
	}
}
