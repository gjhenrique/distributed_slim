package br.uel.cross.parallel.mams.pagemanager;

import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.util.NodeSimpleFactory;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.util.Date;
import java.util.List;

public class HdfsPageManager implements PageManager{

	private FileSystem fs;

	private Serializer<List<EntryTree>> serializer;
	
	private static Path ROOT = new Path("ROOT");

	public HdfsPageManager(FileSystem fs, Serializer<List<EntryTree>> serializer){

		this.fs = fs;
		this.serializer = serializer;
	} 

	@Override
	public NodeTree findRoot(){

		try {
			if(! fs.exists(ROOT)){

				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		long blockId = this.getRootBlockId();

		System.out.println("Raiz está no arquivo " + blockId);

		return this.fromFileToNode(blockId);
	}

	public long getBlockId(){
		return new Date().getTime();

	}

	public void changeRootNode(long blockId, int heigth){

		try {
			DataOutputStream dataOutputStream = this.fs.create(ROOT);

			dataOutputStream.writeLong(blockId);
			dataOutputStream.writeInt(heigth);

			dataOutputStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void writeNodeFileSystem(Path path, NodeTree nodeTree) {

		System.out.println("Escrevendo bloco " + nodeTree.getBlockId() + " no disco");

		OutputStream outputStream = null;
		ObjectOutputStream objectOutputStream = null;
		BufferedOutputStream bufferedOutputStream = null;

		try {
			outputStream = this.fs.create(path);

			bufferedOutputStream = new BufferedOutputStream(outputStream);

			byte [] headerFile = new byte[]{ (byte) (nodeTree.isLeaf()?1:0) };
			bufferedOutputStream.write(headerFile);

			objectOutputStream = new ObjectOutputStream(bufferedOutputStream);
			objectOutputStream.writeObject(nodeTree.getEntry());

			bufferedOutputStream.flush();
			outputStream.close();
			bufferedOutputStream.close();
			objectOutputStream.close();	

		}catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public NodeTree fromFileToNode(long blockId){

		NodeTree nodeTree = null;

		DataInputStream in;

		Path path = new Path(blockId+"");
		try {
			
			in = this.fs.open(path);
			BufferedInputStream bufferedInputStream = new BufferedInputStream(in);

			boolean isLeaf = (in.readByte() != 0);
			
			List<EntryTree> entryList = this.serializer.read(bufferedInputStream);

			nodeTree = NodeSimpleFactory.create(isLeaf);
			nodeTree.setBlockId(Long.parseLong(path.getName()));
			nodeTree.setEntry(entryList);

			in.close();
			bufferedInputStream.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return nodeTree;
	}

	private long getRootBlockId(){
		long rootBlockId = -1;

		try {
			DataInputStream dataInputStream = fs.open(HdfsPageManager.ROOT);

			rootBlockId = dataInputStream.readLong();

			dataInputStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return rootBlockId;
	}

	public void writeNode(long blockId, NodeTree node){

		System.out.println("Escrevendo bloco " + node.getBlockId() + " no disco");

		Path path = new Path(Long.toString(blockId));

		writeNodeFileSystem(path, node);
	}

	@Override
	public int getTreeHeight(){

		int height = 0;

		try {
			DataInputStream data = this.fs.open(ROOT);
			//Lendo desnessário id do bloco
			data.readLong();
			height = data.readInt();
			
			data.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return height;
	}

	@Override
	public boolean pageExists(long blockId) {
		return false;
	}

    @Override
    public void deleteIndex() {
       throw new NotImplementedException();
    }
}
