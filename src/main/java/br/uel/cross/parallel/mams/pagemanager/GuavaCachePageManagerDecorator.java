package br.uel.cross.parallel.mams.pagemanager;

import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;

import br.uel.cross.parallel.mams.model.NodeTree;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheStats;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

public class GuavaCachePageManagerDecorator extends PageManagerDecorator {

	private LoadingCache<Long, NodeTree> guavaCache;
	
	private int numberOfElements;
	
	public GuavaCachePageManagerDecorator(PageManager pageManager, int elements) {
		super(pageManager);

		this.numberOfElements = elements;
		loadCache();
	}

	private void loadCache() {

		RemovalListener<Long, NodeTree> removalListener = this.new RemovalNodeListener(pageManager);

		guavaCache = CacheBuilder.newBuilder().maximumSize(numberOfElements).recordStats()
				.removalListener(removalListener).build(new CacheLoader<Long, NodeTree>() {

					@Override
					public NodeTree load(Long key) throws Exception {
						NodeTree node = GuavaCachePageManagerDecorator.this.pageManager.fromFileToNode(key);
						return node;
					}
				});
	}

	public void flushCache() {

		ConcurrentMap<Long, NodeTree> cacheMap = guavaCache.asMap();
		for (Entry<Long, NodeTree> entry : cacheMap.entrySet()) {

			long blockId = entry.getKey();
			NodeTree nodeTree = entry.getValue();
			this.pageManager.writeNode(blockId, nodeTree);
		}
	}

	public CacheStats getStats() {
		return guavaCache.stats();
	}

	@Override
	public void writeNode(long blockId, NodeTree node) {
		
		if(!this.pageManager.pageExists(blockId)){
			this.pageManager.writeNode(blockId, node);
		}

		try {
			NodeTree nodeCache = guavaCache.get(blockId);

			if (nodeCache.hashCode() != node.hashCode()) {

				this.guavaCache.put(blockId, node);

			}

		} catch (ExecutionException e) {
			e.printStackTrace();
		}

	}

	@Override
	public NodeTree fromFileToNode(long key) {

		NodeTree node = null;
		
		try {
			node = guavaCache.get(key);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		return node;
	}

	private class RemovalNodeListener implements RemovalListener<Long, NodeTree> {

		private PageManager pageManager;
		public RemovalNodeListener(PageManager pageManager) {
			this.pageManager = pageManager;
		}

		@Override
		public void onRemoval(RemovalNotification<Long, NodeTree> notification) {

			long blockId = notification.getKey();
			NodeTree nodeTree = notification.getValue();
			pageManager.writeNode(blockId, nodeTree);
		}

	}

}
