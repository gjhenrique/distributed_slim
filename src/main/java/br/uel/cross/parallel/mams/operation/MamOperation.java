package br.uel.cross.parallel.mams.operation;

import br.uel.cross.parallel.mams.MamTree;
import com.googlecode.jcsv.reader.CSVReader;
import com.googlecode.jcsv.reader.internal.CSVReaderBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

public abstract class MamOperation {

<<<<<<< Updated upstream
	protected MAMTree tree;
=======
    private static final String SPLIT_STRING = " ";
    protected MamTree tree;
    protected long interval;
>>>>>>> Stashed changes

	protected long interval;

<<<<<<< Updated upstream
	private static final String SPLIT_STRING = " ";
=======
    public MamOperation(MamTree mamTree) {
        this.tree = mamTree;
    }
>>>>>>> Stashed changes

	// protected QueryStats queryStats = new QueryStats(this.fileStatsName());

	protected int queryNumber = 0;

	public MamOperation(MAMTree mamTree) {
		this.tree = mamTree;
	}

	public void performAction(String file) {

		Reader reader = null;
		try {
			reader = new FileReader(file);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		CSVReader<String[]> csvParser = CSVReaderBuilder.newDefaultReader(reader);

		List<String[]> data = null;
		try {
			data = csvParser.readAll();
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.processElements(data);
	}

	private void processElements(List<String[]> data) {

		for (String[] pontos : data) {

			List<String> pointsItStrings = Arrays.asList(pontos[0].split(SPLIT_STRING));
			float[] pointsItDouble = new float[pointsItStrings.size()];

			int i = 0;
			for (String point : pointsItStrings) {

				try {
					float pointDouble = Float.parseFloat(point);
					pointsItDouble[i] = pointDouble;
				} catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
				i++;
			}
			this.performActionMam(pointsItDouble);

			queryNumber++;
		}
	}

	protected abstract void performActionMam(float[] points);

}
