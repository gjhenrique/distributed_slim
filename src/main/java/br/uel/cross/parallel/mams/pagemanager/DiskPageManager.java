package br.uel.cross.parallel.mams.pagemanager;

import br.uel.cross.parallel.mams.model.EntryTree;
import br.uel.cross.parallel.mams.model.NodeTree;
import br.uel.cross.parallel.util.NodeSimpleFactory;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;

public class DiskPageManager implements PageManager {

	private long INVALID_ID = -1;

	private static int HEADER_SIZE = 20;

	private RandomAccessFile randomIndexFile;

	private FileChannel channel;

	private Serializer<List<EntryTree>> serializer;

    private File indexFile;

	private int defaultBlockSize;

	private long rootId = INVALID_ID;

	private int height;

	private int usedPages;

	public DiskPageManager(String fileName, Serializer serializer, int defaultBlockSize) {

		try {
			new FileWriter(fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.initializeFile(fileName);
		this.serializer = serializer;
		this.defaultBlockSize = defaultBlockSize;
	}

	public DiskPageManager(String fileName, Serializer serializer) {

		this.initializeFile(fileName);
		this.serializer = serializer;

		this.initializeHeader();
		// Pega id do tamanho padrão do bloco (Pega do Header já criado)
	}

	private void initializeFile(String file) {
		try {
            this.indexFile = new File(file);
			this.randomIndexFile = new RandomAccessFile(file, "rw");
			this.channel = randomIndexFile.getChannel();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

	private void initializeHeader() {
		try {
			this.randomIndexFile.seek(0);

			this.rootId = this.randomIndexFile.readLong();
			this.height = this.randomIndexFile.readInt();
			this.defaultBlockSize = this.randomIndexFile.readInt();
			this.usedPages = this.randomIndexFile.readInt();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public NodeTree findRoot() {
		if (rootId == INVALID_ID) {
			return null;
		}

		long rootId = getRootBlockId();

		return this.fromFileToNode(rootId);
	}

	private long getRootBlockId() {

		long rootBlockId = getBlockId();

		try {
			this.randomIndexFile.seek(0);
			rootBlockId = this.randomIndexFile.readLong();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return rootBlockId;
	}

	@Override
	public long getBlockId() {
		return usedPages++;
	}

	@Override
	public void writeNode(long blockId, NodeTree nodeTree) {

		byte[] bytesPage = new byte[this.defaultBlockSize];
		ByteArrayOutputStream bos = new ByteArrayOutputStream(this.defaultBlockSize);

		bos.write(nodeTree.isLeaf() ? 0 : 1);
		this.serializer.write(bos, nodeTree.getEntry());

		byte[] bytesObject = bos.toByteArray();

		System.arraycopy(bytesObject, 0, bytesPage, 0, bytesObject.length);

		ByteBuffer buffer = ByteBuffer.wrap(bytesPage);

		long offset = this.castIdToOffset(blockId);

		try {
			this.channel.write(buffer, offset);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private long castIdToOffset(long blockId) {
		return blockId * defaultBlockSize + HEADER_SIZE;
	}

	@Override
	public void changeRootNode(long blockId, int height) {

		try {
			this.randomIndexFile.seek(0);
			this.randomIndexFile.writeLong(blockId);
			this.randomIndexFile.writeInt(height);
			this.randomIndexFile.writeInt(defaultBlockSize);
			this.randomIndexFile.writeInt(usedPages);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public NodeTree fromFileToNode(long blockId) {

		long offset = castIdToOffset(blockId);

		NodeTree nodeTree = null;

		try {

			ByteBuffer buffer = ByteBuffer.allocateDirect(defaultBlockSize);
			this.channel.read(buffer, offset);

			buffer.rewind();

			byte[] bytes = new byte[defaultBlockSize];
			buffer.get(bytes);
			ByteArrayInputStream bais = new ByteArrayInputStream(bytes);

			int isLeaf = bais.read();

			List<EntryTree> entries = this.serializer.read(bais);

			nodeTree = NodeSimpleFactory.create((isLeaf == 0));
			nodeTree.setBlockId(blockId);
			nodeTree.setEntry(entries);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return nodeTree;
	}

	@Override
	public int getTreeHeight() {
		int height = 0;
		try {
			this.randomIndexFile.seek(0);
			this.randomIndexFile.readLong();
			height = this.randomIndexFile.readInt();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return height;
	}

	@Override
	protected void finalize() throws Throwable {
		this.randomIndexFile.close();
		this.channel.close();
	}

	@Override
	public boolean pageExists(long blockId) {
		boolean exists = false;

		try {
			 exists = (this.castIdToOffset(blockId) < this.randomIndexFile.length());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return exists;
	}

    @Override
    public void deleteIndex() {
        this.indexFile.delete();
    }
}
