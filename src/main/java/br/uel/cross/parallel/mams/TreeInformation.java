package br.uel.cross.parallel.mams;

public class TreeInformation {
	
	private int height;
	
	private int minHeight;
	
	private int objectSizeSum;
	
	private int objectCount;
	
	private int objectSizeCount;
	
	private double fatFactor;
	
	private double bloatFactor;
	
	private LevelInfo[] levelData;
	
	private boolean ready;
	
	public TreeInformation(int height, int objectCount){
		
		this.height = height;
		
		this.minHeight = height;
		
		this.levelData = new LevelInfo[height];
		
		this.resetData();
		this.invalidate();
		
		this.objectSizeCount = objectCount;
	}

	private void resetData() {
		
//		TODO memset
		
		this.objectSizeSum = 0;
		this.objectSizeCount = 0;
	}

	private void invalidate() {
		this.ready = false;
	}
	
	public void calculate(){
		
		int numIntersections = 0;
		int numNodes = 0;
		
		for(int i = 0; i < height; i++){
			
		}
	}
	
}
