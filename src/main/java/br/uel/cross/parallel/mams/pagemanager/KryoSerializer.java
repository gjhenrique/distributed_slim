package br.uel.cross.parallel.mams.pagemanager;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import br.uel.cross.parallel.mams.model.EntryTree;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class KryoSerializer<T> implements Serializer<T>{

	private Kryo kryo;
	
	/**
	 * Classe que irá ser serializada
	 * O kryo exige um object Class para ler o objeto
	 */
	private Class<T> clazz;
	
	public KryoSerializer(Kryo kryo, Class<T> clazz){
		this.kryo = kryo;
		this.clazz= clazz;
		
		this.registerClasses();
	}
	
	private void registerClasses() {
		kryo.register(EntryTree.class, new EntryTreeSerializer());
		kryo.register(List.class);
		kryo.register(ArrayList.class, new ListSerializer());
		kryo.register(float[].class);		
	}

	@Override
	public void write(OutputStream stream, T object) {
		
		Output output = new Output(stream);
		kryo.writeObject(output, object);
		output.close();
		
	}

	@Override
	public T read(InputStream stream) {
		
		Input input = new Input(stream);
				
		T object = kryo.readObject(input, clazz);
		
		return  object;
	}	
}
