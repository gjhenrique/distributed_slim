package br.uel.cross.parallel.mams.subtreechooser;

import br.uel.cross.parallel.mams.distance.MetricEvaluator;
import br.uel.cross.parallel.mams.model.IndexNodeTree;

public interface SubTreeChooser {

	public int subtreeChooser(IndexNodeTree node, float[] element,
			MetricEvaluator metricEvaluator);
	
}
