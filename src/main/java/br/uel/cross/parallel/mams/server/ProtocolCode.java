package br.uel.cross.parallel.mams.server;

public enum ProtocolCode {

	INITIALRANGEQUERY(1), PARTIALRANGEQUERY(2), KNEARESTQUERY(3), CLOSECONNECTION(4), ASK_FOR_STATS(5);

	private final int codeInt;

	ProtocolCode(int code){
		this.codeInt = code;
	}
	public int getProtocolCode(){
		return this.codeInt;
	}

	public static ProtocolCode getCode(int value){

		for(ProtocolCode protocolCode : ProtocolCode.values()){
			if(value == protocolCode.getProtocolCode()){
				return protocolCode;
			}
		}
		return null;
	}

}
