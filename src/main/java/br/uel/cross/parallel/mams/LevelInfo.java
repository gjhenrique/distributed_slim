package br.uel.cross.parallel.mams;

public class LevelInfo {

	private int intersections;
	
    private int nodeCount;

    private int objectCount;

    private double fatFactor;

	public int getIntersections() {
		return intersections;
	}

	public void setIntersections(int intersections) {
		this.intersections = intersections;
	}

	public int getNodeCount() {
		return nodeCount;
	}

	public void setNodeCount(int nodeCount) {
		this.nodeCount = nodeCount;
	}

	public int getObjectCount() {
		return objectCount;
	}

	public void setObjectCount(int objectCount) {
		this.objectCount = objectCount;
	}

	public double getFatFactor() {
		return fatFactor;
	}

	public void setFatFactor(double fatFactor) {
		this.fatFactor = fatFactor;
	}
	
}
