package br.uel.cross.parallel.mams.result;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;


public abstract class Result {
	
	protected QueryType queryType;
	
	protected int k;
	
	protected boolean tie;
	
	protected double radius;
	
	protected double innerRadius;
	
	public QueryType getQueryType(){
		return queryType;
	}
	
	public int getK(){
		return k;
	}

	public boolean isTie() {
		return tie;
	}

	public double getInnerRadius() {
		return innerRadius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	protected static class CompareKeysDistance implements Comparator<ResultPair>, Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -6173233485315836057L;

		@Override
		public int compare(ResultPair o1, ResultPair o2) {

			if(o1.getDistance() == o2.getDistance())
				return -1;
			
			return o1.getDistance() < o2.getDistance() ? -1
		            : o1.getDistance() > o2.getDistance() ? 1
		            : 0;
		}

		
	}
	
	public abstract Collection<ResultPair> getPairs();
	
	public abstract void mergeResult(Result result);
}
