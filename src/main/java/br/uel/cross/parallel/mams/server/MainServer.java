package br.uel.cross.parallel.mams.server;

import java.util.ArrayList;

import org.apache.hadoop.fs.FileSystem;

import br.uel.cross.parallel.mams.Main;
import br.uel.cross.parallel.mams.pagemanager.FilePageManager;
import br.uel.cross.parallel.mams.pagemanager.HdfsPageManager;
import br.uel.cross.parallel.mams.pagemanager.KryoSerializer;
import br.uel.cross.parallel.mams.pagemanager.PageManager;
import br.uel.cross.parallel.mams.pagemanager.Serializer;
import br.uel.cross.parallel.util.PropertiesUtil;

import com.esotericsoftware.kryo.Kryo;

public class MainServer {

	private static final String SERVER_FILE = "server.properties";

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		//		Caminho do Hdfs
		String uri = PropertiesUtil.getValueFromConfiguration("paths.properties", "HDFS");

		//		Caminho Local
		String local = PropertiesUtil.getValueFromConfiguration("paths.properties", "MAM");

		System.out.println("Inicializando servidor");

		//		TODO Colocar num factory
		String pageManagerString = PropertiesUtil.getValueFromConfiguration("server.properties",
				"page_manager");

		Kryo kryo = new Kryo();

		Serializer serializer = new KryoSerializer<ArrayList>(kryo, ArrayList.class);

		PageManager pageManager = null;
		if(pageManagerString.equals("Hdfs")){
//			FileSystem fs = Main.loadHdfs(uri);
//			pageManager = new HdfsPageManager(fs, serializer);
		}
		else if(pageManagerString.equals("Local")){

//			pageManager = new FilePageManager(local, Main.getDefaultBlockSizeFromHdfs(), serializer);
		}
		else{
			throw new RuntimeException("Argumento do Page Manager inválido");
		}

		int port = PropertiesUtil.getIntFromConfiguration("network.properties", "PORT-SERVER");
		int numberOfThreads = PropertiesUtil.getIntFromConfiguration("server.properties", "threads_number");

		SocketServer server = new SocketServer(pageManager, port, numberOfThreads);
		server.acceptNewConnections();

	}
}
