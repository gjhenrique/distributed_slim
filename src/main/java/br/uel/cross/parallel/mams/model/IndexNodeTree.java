package br.uel.cross.parallel.mams.model;


public class IndexNodeTree extends NodeTree{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1077871467828837525L;
	
	public IndexNodeTree(){
		super();
	}
	
	public IndexNodeTree(long fileId) {
		super(fileId);
	}
	
	@Override
	public boolean isLeaf() {
		return false;
	}

	@Override
	public EntryTree createEntry() {
		return new IndexEntryTree();
	}

	@Override
	public double getMinimumRadius() {
		
		double distance, minRadius = 0;
		
		for (int i = 0; i < getNumberOfEntries(); i++){
			distance = getEntry().get(i).getRepresentativeDistance() + 
					((IndexEntryTree)getEntry().get(i)).getConveringRadius();
			if (minRadius < distance){
				minRadius = distance;
			}
		}

		return minRadius;
	}
	
	public int getTotalObjectCount(){

		int count = 0;
		for (int i = 0; i < getNumberOfEntries(); i++){
			count += ((IndexEntryTree)getEntry().get(i)).getnEntries();
		}
		
		return count;
	}
	
}
