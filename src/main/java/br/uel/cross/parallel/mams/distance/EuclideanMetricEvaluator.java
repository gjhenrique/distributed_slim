package br.uel.cross.parallel.mams.distance;


public class EuclideanMetricEvaluator implements MetricEvaluator{

	@Override
	public double getDistance(float[] obj1, float[] obj2) {
		
		double value = 0;
		double tmp;
		
		for(int i = 0; i < obj1.length; i++){
			
			tmp = obj1[i] - obj2[i];
			value = value + (tmp * tmp);
		}

		return Math.sqrt(value);
	}
}