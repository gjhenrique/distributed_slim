package br.uel.cross.parallel.mams.result;

import java.util.HashMap;
import java.util.Map;

public class PruningInfo {

	private Map<Integer, Integer> indexPruned = new HashMap<Integer, Integer>();
	private Map<Integer, Integer> indexNonPruned = new HashMap<Integer, Integer>();

	private Map<Integer, Integer> leafPruned = new HashMap<Integer, Integer>();
	private Map<Integer, Integer> leafNonPruned = new HashMap<Integer, Integer>();

	private int treeHeight;

	public PruningInfo(int treeHeight) {

		this.treeHeight = treeHeight;
		this.intializeMap();
	}

	private void intializeMap() {

		for (int i = 1; i <= treeHeight - 1; i++) {
			indexPruned.put(i, 0);
			indexNonPruned.put(i, 0);
		}

		leafPruned.put(treeHeight, 0);
		leafNonPruned.put(treeHeight, 0);
	}

	public void addIndexPruning(int height) {
		addValueMap(height, indexPruned);
	}

	public void addIndexNonPruning(int height) {
		addValueMap(height, indexNonPruned);
	}

	public void addLeafPruning(int height) {
		addValueMap(height, leafPruned);
	}

	public void addLeafNonPruning(int height) {
		addValueMap(height, leafNonPruned);
	}

	private void addValueMap(int height, Map<Integer, Integer> map) {

		this.validateHeight(height);
		int value = map.get(height);
		map.put(height, value+1);

	}

	private void validateHeight(int height) {
		if (height <= 0)
			throw new IllegalArgumentException("Altura nao pode ser menor ou igual a " + 0);

		if (height > treeHeight)
			throw new IllegalArgumentException("Altura nao pode ser maior que " + treeHeight);

	}

	public String getStatsPruning() {

		StringBuilder builder = new StringBuilder();
		
		builder.append("PODA / NÃO PODA\n");
		for (int i = 1; i <= treeHeight - 1; i++) {
			builder.append("Altura Indice " + i + " => " + indexPruned.get(i) + " / " + indexNonPruned.get(i)+ "\n");
		}

		builder.append("Altura Folha " + treeHeight + " => " + leafPruned.get(treeHeight) + "/"
				+ leafNonPruned.get(treeHeight));
		
		return builder.toString();

	}
}
