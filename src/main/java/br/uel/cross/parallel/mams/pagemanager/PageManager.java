package br.uel.cross.parallel.mams.pagemanager;

import br.uel.cross.parallel.mams.model.NodeTree;

public interface PageManager{
	
	public NodeTree findRoot();
	
	public long getBlockId();
	
	public void writeNode(long blockId, NodeTree nodeTree);
	
	public void changeRootNode(long blockId, int height);
	
	public NodeTree fromFileToNode(long blockId);
	
	public int getTreeHeight();
	
	public boolean pageExists(long blockId);

    public void deleteIndex();

}